
exports.false2NullString = function(string){
    if(string){
       return string;
    }else{
       return '';
    }
};
exports.oneDigit2TowDigits = function(number){
    if(number < 10){
        return '0' + number;
    } else{
        return number;
    }
};
exports.formatTime2YMDHMS = function(time){
    return  time.getFullYear() + '-' +
        exports.oneDigit2TowDigits(time.getMonth() + 1) +  '-' +
        exports.oneDigit2TowDigits(time.getDate()) +' ' +
        exports.oneDigit2TowDigits(time.getHours()) + ':' +
        exports.oneDigit2TowDigits(time.getMinutes()) + ':' +
        exports.oneDigit2TowDigits(time.getSeconds());
};
exports.regResetPasswordToken = function(token){
    var regexp = new RegExp('^[a-f0-9]{88}$');
    if (regexp.test(token)) {
        return true;
    }else{
        return false;
    }
};
exports.regRegisterToken = function(token){
    var regexp = new RegExp('^[a-f0-9]{88}$');
    if (regexp.test(token)) {
        return true;
    }else{
        return false;
    }
};
exports.regMongoDBId = function(id){
    var regexp = new RegExp('^[a-f0-9]{24}$');
    if (regexp.test(id)) {
        return true;
    }else{
        return false;
    }
};