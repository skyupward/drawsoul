
exports.randomString = function(length){
    var sample = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' +
        ',./<>?;\':\"[]{}!@#$%^&*()_=/*-+`~\\|';
    var result = "";
    var sampleLength = sample.length;
    for(var i = 0 ; i < length; i++){
        result += sample[Math.floor(Math.random() * sampleLength)];
    }
    return result;
}

exports.randomHex = function(length){
    var sample = 'ABCDEFabcdef0123456789';
    var result = "";
    var sampleLength = sample.length;
    for(var i = 0 ; i < length; i++){
        result += sample[Math.floor(Math.random() * sampleLength)];
    }
    return result;
}