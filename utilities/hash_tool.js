
var createHmac = require('crypto').createHmac;
var createHash = require('crypto').createHash;
exports.hmacSha256Hex = function(key, data){
    return createHmac('sha256', key).update(data).digest('hex');
}
exports.hashSha256Hex = function(data){
    return createHash('sha256').update(data).digest('hex');
}