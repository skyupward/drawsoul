/*
 * Router
 */
var views = require('../views');
var auth = views.auth;
var index = views.index;
var user = views.user;
var drawing = views.drawing;
var album = views.album;
var settings = views.settings;
var message = views.message;
var attention = views.attention;
var like = views.like;
var collection = views.collection;
var personalChat = views.personalChat;
var tag = views.tag;
var info = views.info;

module.exports = function (app) {

    //index
    app.get('/', index.indexGET);

    //auth
    app.get('/login', auth.loginGET);
    app.post('/login', auth.loginPOST);
    app.get('/register', auth.registerGET);
    app.post('/register', auth.registerPOST);
    app.get('/logout', loginRequired, auth.logoutGET);
    app.get('/welcome/to/xxx/:token', auth.activateAccountGET);
    app.get('/forget/password', auth.forgetPasswordGET);
    app.post('/forget/password', auth.forgetPasswordPOST);
    app.get('/reset/password/:token', auth.resetPasswordGET);
    app.post('/reset/password/:token', auth.resetPasswordPOST);
//    app.get('/api/captcha', auth.captchaGET);

    //user
    app.get('/artists', user.showListGET);
    app.get('/attentions/:_id', user.showAttentionsGET);
    app.get('/fans/:_id', user.showFansGET);
    app.get('/artist/:_id', user.showOneGET);

    //drawing
    app.get('/drawings/hot', drawing.hotGET);
    app.get('/drawing/new', loginRequired, drawing.newGET);
    app.post('/drawing/new', loginRequired, drawing.newPOST);
    app.post('/drawing/delete', loginRequired, drawing.deletePOST);
    app.get('/drawing/edit/:_id', loginRequired, drawing.editGET);
    app.post('/drawing/edit/:_id', loginRequired, drawing.editPOST);
    app.get('/drawing/:_id', drawing.showOneGET);
    app.post('/drawing/:_id', loginRequired, drawing.showOnePOST);

    //album
    app.get('/album/new', loginRequired, album.newGET);
    app.post('/album/new', loginRequired, album.newPOST);
    app.post('/album/delete', loginRequired, album.deletePOST);
    app.get('/album/edit/:_id', loginRequired, album.editGET);
    app.post('/album/edit/:_id', loginRequired, album.editPOST);
    app.get('/album/:_id', album.showOneGET);

    //settings
    app.get('/set/basic', loginRequired, settings.setBasicGET);
    app.post('/set/basic', loginRequired, settings.setBasicPOST);
    app.get('/set/extra', loginRequired, settings.setExtraGET);
    app.post('/set/extra', loginRequired, settings.setExtraPOST);
    app.get('/set/password', loginRequired, settings.setPasswordGET);
    app.post('/set/password', loginRequired, settings.setPasswordPOST);
    app.get('/set/portrait', loginRequired, settings.setPortraitGET);
    app.post('/set/portrait', loginRequired, settings.setPortraitPOST);

    //message
    app.get('/message/drawing', loginRequired, message.drawingCommentGET);
    app.post('/message/drawing', loginRequired, message.drawingCommentPOST);
    app.get('/message/at', loginRequired, message.atGET);
    app.post('/message/at', loginRequired, message.atPOST);
    app.get('/message/chat', loginRequired, message.personalChatGET);
    app.post('/message/chat', loginRequired, message.personalChatPOST);
    app.get('/message/set/read', loginRequired, message.setReadGET);

    //attention
    app.get('/api/attention', loginRequired, attention.attentionGET);
    app.post('/api/attention', loginRequired, attention.attentionPOST);

    //like
    app.get('/api/like', loginRequired, like.likeGET);
    app.post('/api/like', loginRequired, like.likePOST);

    //collection
    app.get('/api/collection', loginRequired, collection.collectionGET);
    app.post('/api/collection', loginRequired, collection.collectionPOST);

    //personal chat
    app.get('/chat/send/:_id', loginRequired, personalChat.sendChatGET);
    app.post('/chat/send/:_id', loginRequired, personalChat.sendChatPOST);
    app.get('/chat/:_id', loginRequired, personalChat.chatListGET);
    app.post('/chat/:_id', loginRequired, personalChat.chatListPOST);

    //tag
    app.get('/tags', tag.getTagsGET);
    app.get('/tag/:_id', tag.queryTagGET);

    //info
//    app.get('/license', info.licenseGET);
    app.get('/about', info.aboutGET);

};

function loginRequired(req, res, next) {
    if (req.session.user) {
        next();
    } else {
        if (req.xhr) {
            res.json({error: true});
        } else {
            res.redirect('/login');
        }
    }
}