
var config = require('../../config');

module.exports = function (req, res, next) {
    res.locals.currentYear = new Date().getFullYear();
    res.locals.appName = config.APP_NAME;
    next();
};