
module.exports = function (req, res) {
    res.status(404);
    if (req.xhr) {
        res.json({error: true});
    } else {
        res.render('../middleware/error_404/error_404', {title:"迷失了"});
    }
};