
module.exports = function (req, res, next) {
    res.locals.success = '';
    res.locals.error = '';
    next();
};