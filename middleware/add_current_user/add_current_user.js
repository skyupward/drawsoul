
module.exports = function (req, res, next) {
    res.locals.currentUser = req.session.user;
    next();
};