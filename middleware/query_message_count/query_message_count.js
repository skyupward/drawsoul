
var DrawingCommentMessageDAO = require('../../DAO').DrawingCommentMessageDAO;
var AtMessageDAO = require('../../DAO').AtMessageDAO;
var PersonalChatMessageDAO = require('../../DAO').PersonalChatMessageDAO;
module.exports = function (req, res, next) {
    if (!req.session.user) {
        next();
        return;
    }
    DrawingCommentMessageDAO.getUnreadMessagesCountByUserId(req.session.user._id, function (err, count) {
        if (err) {
            return next(err);
        }
        res.locals.messageCount = {
            drawingComment: count
        };
        AtMessageDAO.getUnreadMessagesCountByUserId(req.session.user._id, function (err, count) {
            if (err) {
                return next(err);
            }
            res.locals.messageCount.at = count;
            PersonalChatMessageDAO.getUnreadMessagesCountByUserId(req.session.user._id, function (err, count) {
                if (err) {
                    return next(err);
                }
                res.locals.messageCount.personalChat = count;
                var amount = 0;
                for (var key in res.locals.messageCount) {
                    amount += res.locals.messageCount[key];
                }
                res.locals.messageCount.amount = amount;
                next();
            });
        });
    });
};