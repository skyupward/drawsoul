
module.exports = function (req, res, next) {
    res.locals.csrfToken = req.session._csrf;
    next();
};