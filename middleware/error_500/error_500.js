
module.exports = function (err, req, res, next) {
    console.error(err);
    res.status(500);
    if (req.xhr) {
        res.json({error: true});
    } else {
        res.render('../middleware/error_500/error_500', {title:"犯错了"});
    }
};