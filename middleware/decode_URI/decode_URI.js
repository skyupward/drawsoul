
module.exports = function (req, res, next) {
    req.url = decodeURI(req.url);
    next();
};