
var config = require('../../../config');
var TagDAO = require('../../../DAO').TagDAO;
exports.getTagsGET = function (req, res, next) {
    TagDAO.getCount(function (err, count) {
        if (err) {
            return next(err);
        }
        var pageAmount = 0, quotient = count / config.TAG_AMOUNT_PER_PAGE;
        if (quotient == Math.floor(quotient)) {
            pageAmount = quotient;
        } else {
            pageAmount = Math.floor(quotient) + 1;
        }
        var page = parseInt(req.query.page);
        page = page > 0 && page <= pageAmount ? page : 1;
        TagDAO.findTags(page, config.TAG_AMOUNT_PER_PAGE, function (err, tags) {
            if (err) {
                return next(err);
            }
            var templateData = {
                title: '标签',
                tags: tags,
                currentPage: page,
                pageAmount: pageAmount
            };
            res.render('tag/get_tags/get_tags', templateData);
        });
    });
};