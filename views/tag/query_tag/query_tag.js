
var config = require('../../../config');
var DrawingDAO = require('../../../DAO').DrawingDAO;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var UserDAO = require('../../../DAO').UserDAO;
var TagDAO = require('../../../DAO').TagDAO;
var stringTool = require('../../../utilities').stringTool;

exports.queryTagGET = function (req, res, next) {
    var tagId = req.params._id;
    if (!stringTool.regMongoDBId(tagId)) {
        return next();
    }
    TagDAO.findTagById(tagId, function (err, tag) {
        if (err) {
            return next(err);
        }
        if (tag) {
            var page = parseInt(req.query.page);
            page = page > 0 ? page : 1;
            DrawingDAO.findDrawingsByTag(tag._id, page, 20, function (err, drawings) {
                if (err) {
                    return next(err);
                }
                AlbumDAO.findAlbumsByTag(tag._id, page, 20, function (err, albums) {
                    if (err) {
                        return next(err);
                    }
                    UserDAO.findUsersByTag(tag._id, page, 20, function (err, users) {
                        if (err) {
                            return next(err);
                        }
                        var templateData = {
                            drawings: drawings,
                            albums: albums,
                            users: users
                        };
                        if (req.query.page) {
                            res.render('tag/query_tag/infinite_scroll_page', templateData);
                        } else {
                            templateData.title = "标签";
                            res.render('tag/query_tag/query_tag', templateData);
                        }
                    });
                });
            });
        } else {
            next();
        }
    });
};