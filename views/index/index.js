
var DrawingDAO = require('../../DAO').DrawingDAO;
var config = require('../../config');

var title = "画画爱好者社区";

exports.indexGET = function (req, res, next) {
    if (req.query.page) {
        DrawingDAO.findDrawingsByPage(req.query.page, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
            if (err) {
                return next(err);
            }
            var templateData = {
                drawings: drawings
            }
            res.render('index/infinite_scroll_page', templateData);
        });
    } else {
        DrawingDAO.findDrawingsByPage(1, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
            if (err) {
                return next(err);
            }
            var templateData = {
                title: title,
                drawings: drawings
            }
            res.render('index/index', templateData);
        });
    }
}