
var UserDAO = require('../../../DAO').UserDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var CollectionDAO = require('../../../DAO').CollectionDAO;
var stringTool = require('../../../utilities').stringTool;
var config = require('../../../config');

var title = "的画室";

exports.showOneGET = function (req, res, next) {
    var userId = req.params._id;
    if (!stringTool.regMongoDBId(userId)) {
        return next();
    }
    if (req.query.page) {
        if (req.query.album === "") {
            AlbumDAO.findAlbumsByUserIdByPage(userId, req.query.page, config.ALBUM_AMOUNT_PER_PAGE, function (err, albums) {
                if (err) {
                    return next(err);
                }
                var templateData = {
                    albums: albums
                }
                res.render('user/show_one/infinite_scroll_page', templateData);
            });
        } else if (req.query.collection === "") {
            if (req.session.user && userId === req.session.user._id) {//only current user can say collection
                CollectionDAO.findCollectionsByUserIdByPage(userId, req.query.page, config.COLLECTION_AMOUNT_PER_PAGE, function (err, collections) {
                    if (err) {
                        return next(err);
                    }
                    var templateData = {
                        collections: collections
                    }
                    res.render('user/show_one/infinite_scroll_page', templateData);
                });
            } else {
                next();
            }
        } else {
            DrawingDAO.findDrawingsByUserIdByPage(userId, req.query.page, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
                if (err) {
                    return next(err);
                }
                var templateData = {
                    drawings: drawings
                }
                res.render('user/show_one/infinite_scroll_page', templateData);
            });
        }
    } else {
        UserDAO.findUserById(userId, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                var templateData = {
                    title: user.name + title,
                    host: user
                }
                if (req.query.album === "") {
                    AlbumDAO.findAlbumsByUserIdByPage(user._id, 1, config.ALBUM_AMOUNT_PER_PAGE, function (err, albums) {
                        if (err) {
                            return next(err);
                        }
                        templateData.albums = albums;
                        res.render('user/show_one/show_one', templateData);
                    });
                } else if (req.query.collection === "") {
                    if (req.session.user && user.id === req.session.user._id) {//only current user can say collection
                        CollectionDAO.findCollectionsByUserIdByPage(user._id, 1, config.COLLECTION_AMOUNT_PER_PAGE, function (err, collections) {
                            if (err) {
                                return next(err);
                            }
                            templateData.collections = collections;
                            res.render('user/show_one/show_one', templateData);
                        });
                    } else {
                        next();
                    }
                } else {
                    DrawingDAO.findDrawingsByUserIdByPage(user._id, 1, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
                        if (err) {
                            return next(err);
                        }
                        templateData.drawings = drawings;
                        res.render('user/show_one/show_one', templateData);
                    });
                }
            } else {
                next();
            }
        });
    }
};