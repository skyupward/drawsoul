
var settings = require('./.');

exports.showListGET = require('./show_list').showListGET;
exports.showAttentionsGET = require('./show_attentions').showAttentionsGET;
exports.showFansGET = require('./show_fans').showFansGET;
exports.showOneGET = require('./show_one').showOneGET;
