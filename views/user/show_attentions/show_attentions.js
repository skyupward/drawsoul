
var UserDAO = require('../../../DAO').UserDAO;
var AttentionDAO = require('../../../DAO').AttentionDAO;
var config = require('../../../config');
var stringTool = require('../../../utilities').stringTool;

var title = '的关注';

exports.showAttentionsGET = function (req, res, next) {
    var userId = req.params._id;
    if (!stringTool.regMongoDBId(userId)) {
        return next();
    }
    if (req.query.page) {
        AttentionDAO.findAttentionsByUserIdByPage(userId, req.query.page, config.PAINTER_AMOUNT_PER_PAGE, function (err, attentions) {
            if (err) {
                return next(err);
            }
            var templateData = {
                attentions: attentions
            };
            res.render('user/show_attentions/infinite_scroll_page', templateData);
        });
    } else {
        UserDAO.findUserById(userId, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                AttentionDAO.findAttentionsByUserIdByPage(user._id, 1, config.PAINTER_AMOUNT_PER_PAGE, function (err, attentions) {
                    if (err) {
                        return next(err);
                    }
                    var templateData = {
                        title: user.name + title,
                        attentions: attentions
                    };
                    res.render('user/show_attentions/show_attentions', templateData);
                });
            } else {
                next();
            }
        });
    }
}