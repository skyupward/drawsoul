
var UserDAO = require('../../../DAO').UserDAO;
var config = require('../../../config');

var title = "画家";

exports.showListGET = function (req, res, next) {
    if (req.query.page) {
        UserDAO.findUsersByPage(req.query.page, config.PAINTER_AMOUNT_PER_PAGE, function (err, users) {
            if (err) {
                return next(err);
            }
            var templateData = {
                users: users
            };
            res.render('user/show_list/infinite_scroll_page', templateData);
        });
    } else {
        UserDAO.findUsersByPage(1, config.PAINTER_AMOUNT_PER_PAGE, function (err, users) {
            if (err) {
                return next(err);
            }
            var templateData = {
                title: title,
                users: users
            };
            res.render('user/show_list/show_list', templateData);
        });
    }
}