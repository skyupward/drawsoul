
var config = require('../../../config');
var AtMessageDAO = require('../../../DAO').AtMessageDAO;
var stringTool = require('../../../utilities').stringTool;

var title = '@我的消息';

exports.atGET = function (req, res, next) {
    if (req.query.read === "") {
        AtMessageDAO.getReadMessagesCountByUserId(req.session.user._id, function (err, count) {
            if (err) {
                return next(err);
            }
            var pageAmount = 0, quotient = count / config.MESSAGE_AMOUNT_PER_PAGE;
            if (quotient == Math.floor(quotient)) {
                pageAmount = quotient;
            } else {
                pageAmount = Math.floor(quotient) + 1;
            }
            var page = parseInt(req.query.page);
            page = page > 0 && page <= pageAmount ? page : 1;
            AtMessageDAO.findReadMessagesByUserId(req.session.user._id, page, config.MESSAGE_AMOUNT_PER_PAGE, function (err, messages) {
                if (err) {
                    return next(err);
                }
                var templateData = {
                    title: title,
                    read: true,
                    atMessages: messages,
                    currentPage: page,
                    pageAmount: pageAmount
                };
                res.render('message/at/at', templateData);
            });
        });
    } else {
        AtMessageDAO.getUnreadMessagesCountByUserId(req.session.user._id, function (err, count) {
            if (err) {
                return next(err);
            }
            var pageAmount = 0, quotient = count / config.MESSAGE_AMOUNT_PER_PAGE;
            if (quotient == Math.floor(quotient)) {
                pageAmount = quotient;
            } else {
                pageAmount = Math.floor(quotient) + 1;
            }
            var page = parseInt(req.query.page);
            page = page > 0 && page <= pageAmount ? page : 1;
            AtMessageDAO.findUnreadMessagesByUserId(req.session.user._id, page, config.MESSAGE_AMOUNT_PER_PAGE, function (err, messages) {
                if (err) {
                    return next(err);
                }
                var templateData = {
                    title: title,
                    unread: true,
                    atMessages: messages,
                    currentPage: page,
                    pageAmount: pageAmount
                };
                res.render('message/at/at', templateData);
            });
        });
    }
};

exports.atPOST = function (req, res, next) {
    var form = req.body.form;
    if (form) {
        var messages = form.messages;
        if ((typeof messages == 'object') && messages.constructor == Array) {
            //delete multiple selected
            var messagesSize = messages.length;

            for (var i = 0; i < messagesSize; i++) {
                if (!stringTool.regMongoDBId(messages[i])) {
                    return next();
                }
            }

            function done() {
                if (--messagesSize) {
                    return;
                }
                res.redirect('back');
            }

            messages.forEach(function (message) {
                AtMessageDAO.removeMessageById(message, function (err) {
                    if (err) {
                        return next(err);
                    }
                    done();
                });
            });
        } else {
            //delete single selected
            if (!stringTool.regMongoDBId(messages)) {
                return next();
            }
            AtMessageDAO.removeMessageById(messages, function (err) {
                if (err) {
                    return next(err);
                }
                res.redirect('back');
            });
        }
    } else {
        //delete all
        AtMessageDAO.removeMessagesByToUserId(req.session.user._id, function (err) {
            if (err) {
                return next(err);
            }
            res.redirect('back');
        });
    }
};

