
var DrawingCommentMessageDAO = require('../../../DAO').DrawingCommentMessageDAO;
var PersonalChatMessageDAO = require('../../../DAO').PersonalChatMessageDAO;
var AtMessageDAO = require('../../../DAO').AtMessageDAO;
var stringTool = require('../../../utilities').stringTool;

exports.setReadGET = function (req, res, next) {
    var atMessageId = req.query.am;
    var drawingCommentMessageId = req.query.dm;
    var personalChatMessageId = req.query.cm;
    if (atMessageId) {
        if (!stringTool.regMongoDBId(atMessageId)) {
            return next();
        }
        AtMessageDAO.findMessageByIdAndSetRead(atMessageId, function (err, message) {
            if (err) {
                return next(err);
            }
            if(!message){
                return next();
            }
            var drawingId = req.query.d;
            if (drawingId) {
                res.redirect('/drawing/' + req.query.d + '?c=' + req.query.c + '#say');
            } else {
                next();
            }

        });
    } else if (drawingCommentMessageId) {
        if (!stringTool.regMongoDBId(drawingCommentMessageId)) {
            return next();
        }
        DrawingCommentMessageDAO.findMessageByIdAndSetRead(drawingCommentMessageId, function (err, message) {
            if (err) {
                return next(err);
            }
            if(!message){
                return next();
            }
            var drawingId = req.query.d;
            if (drawingId) {
                res.redirect('/drawing/' + req.query.d + '?c=' + req.query.c + '#say');
            } else {
                next();
            }
        });
    } else if (personalChatMessageId) {
        if (!stringTool.regMongoDBId(personalChatMessageId)) {
            return next();
        }
        PersonalChatMessageDAO.findMessageByIdAndSetRead(personalChatMessageId, function (err, message) {
            if (err) {
                return next(err);
            }
            if(!message){
                return next();
            }
            var userId = req.query.u;
            if (userId) {
                res.redirect('/chat/' + userId);
            } else {
                next();
            }
        });
    } else {
        next();
    }
};
