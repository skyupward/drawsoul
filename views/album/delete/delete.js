
var AlbumDAO = require('../../../DAO').AlbumDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var DrawingCommentDAO = require('../../../DAO').DrawingCommentDAO;
var DrawingCommentMessageDAO = require('../../../DAO').DrawingCommentMessageDAO;
var AtMessageDAO = require('../../../DAO').AtMessageDAO;
var CollectionDAO = require('../../../DAO').CollectionDAO;
var LikeDAO = require('../../../DAO').LikeDAO;
var UserDAO = require('../../../DAO').UserDAO;
var stringTool = require('../../../utilities').stringTool;
var qiniuService = require('../../../service').qiniuService;

exports.deletePOST = function (req, res, next) {
    var form = req.body.form;
    if (!form || !stringTool.regMongoDBId(form.album)) {
        return next();
    }

    AlbumDAO.findAlbumById(form.album, function(err, album){
        if (err) {
            return next(err);
        }

        if(!album){
            return next();
        }

        DrawingDAO.findDrawingsByAlbumId(form.album, function (err, drawings) {
            if (err) {
                return next(err);
            }

            var drawingSize = drawings.length;

            if (!drawingSize) {
                AlbumDAO.removeAlbumById(form.album, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.redirect('/artist/' + req.session.user._id);
                });
                return;
            }

            function done() {
                if (--drawingSize) {
                    return;
                }
                AlbumDAO.removeAlbumById(form.album, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.redirect('/artist/' + req.session.user._id);
                });
            }

            function removeDrawing(drawingId, path) {
                LikeDAO.removeLikesByDrawingId(drawingId, function (err) {
                    if (err) {
                        return next(err);
                    }
                    CollectionDAO.removeCollectionsByDrawingId(drawingId, function (err) {
                        if (err) {
                            return next(err);
                        }
                        AtMessageDAO.removeMessagesByDrawingId(drawingId, function (err) {
                            if (err) {
                                return next(err);
                            }
                            DrawingCommentMessageDAO.removeMessagesByDrawingId(drawingId, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                DrawingCommentDAO.removeCommentsByDrawingId(drawingId, function (err) {
                                    if (err) {
                                        return next(err);
                                    }
                                    DrawingDAO.removeDrawingById(drawingId, function (err) {
                                        if (err) {
                                            return next(err);
                                        }
                                        UserDAO.decPaintingCountById(req.session.user._id, function (err) {
                                            if (err) return next(err);
                                            qiniuService.deleteImage(path, function () {
                                                done();
                                            }, next);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }

            drawings.forEach(function (drawing) {
                removeDrawing(drawing._id, drawing.path);
            });

        });

    });
}