
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var TagDAO = require('../../../DAO').TagDAO;
var config = require('../../../config');

var title = "新的画册";

exports.newGET = function (req, res) {
    var templateData = {
        title: title
    };
    if(req.query.error === 'name'){
        templateData.error = '请输入画册名,且画册名不超过20个字';
    }else if(req.query.error === 'describe'){
        templateData.error = "描述不超过200个字";
    }else if(req.query.error === 'tags'){
        templateData.error = "每个标签最多15个字，最多贴5个标签";
    }else if(req.query.success === 'ok'){
        templateData.success = "新画册已创建";
    }
    res.render('album/new/new', templateData);
};


exports.newPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var albumRaw = {};

    var name = sanitize(form.name).trim();
    name = sanitize(name).xss();
    try {
        check(name).len(1, 20);
        albumRaw.name = name;
    } catch (e) {
        res.redirect('album/new?error=name');
        return;
    }

    if (form.describe) {
        var describe = sanitize(form.describe).trim();
        describe = sanitize(describe).xss();
        try {
            check(describe).len(1, 200);
            albumRaw.describe = describe;
        } catch (e) {
            res.redirect('album/new?error=describe');
            return;
        }
    }

    if (form.tags) {
        var tags = sanitize(form.tags).trim();
        var tags = tags.split(" ");
        var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in tags
        try {
            if (tags.length > 5) {
                throw new Error('标签数不大于5个');
            }
            for (var i in tags) {
                var tag = sanitize(tags[i]).trim();
                tag = sanitize(tag).xss();
                if (tag.length > 15) {
                    throw new Error('标签字数大于15');
                }
                if (tag.length === 0 || tempObj[tag]) {
                    continue;
                }
                tempObj[tag] = true;
                tempArray.push(tag);
            }
            albumRaw._tags = tempArray;
        } catch (e) {
            res.redirect('album/new?error=tags');
            return;
        }
    }

    //no tags need deal
    if (!albumRaw._tags) {
        albumRaw._creator = req.session.user._id;
        albumRaw.coverPath = 'default';//use qiniu
//        albumRaw.coverPath = config.ALBUM_DEFAULT_PATH;
        AlbumDAO.newAlbum(albumRaw, function (err) {
            if (err) {
                return next(err);
            }
            res.redirect('album/new?success=ok');
        });
        return;
    }

    //else
    //just increase new tags' using count simply,
    //then set new tags to db
    var tagsSize = albumRaw._tags.length;
    var newTagIds = [];

    function done(_id) {
        newTagIds.push(_id);
        if (--tagsSize) {
            return;
        }
        albumRaw._tags = newTagIds;
        albumRaw._creator = req.session.user._id;
        albumRaw.coverPath = 'default';//use qiniu
//        albumRaw.coverPath = config.ALBUM_DEFAULT_PATH;
        AlbumDAO.newAlbum(albumRaw, function (err) {
            if (err) {
                return next(err);
            }
            res.redirect('album/new?success=ok');
        });
    }

    albumRaw._tags.forEach(function (tagName) {
        TagDAO.isExistByName(tagName, function (err, exist) {
            if (err) {
                return next(err);
            }
            if (exist) {
                TagDAO.incUsingCountByName(tagName, function (err, tag) {
                    if (err) {
                        return next(err);
                    }
                    done(tag._id);
                });
            } else {
                var tagRaw = {
                    name: tagName,
                    _creator: req.session.user._id
                };
                TagDAO.newTag(tagRaw, function (err, tag) {
                    if (err) {
                        return next(err);
                    }
                    done(tag._id);
                });
            }
        });
    });
};