
exports.newGET = require('./new').newGET;
exports.newPOST = require('./new').newPOST;
exports.deletePOST = require('./delete').deletePOST;
exports.editGET = require('./edit').editGET;
exports.editPOST = require('./edit').editPOST;
exports.showOneGET = require('./show_one').showOneGET;