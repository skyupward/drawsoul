
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var TagDAO = require('../../../DAO').TagDAO;
var stringTool = require('../../../utilities').stringTool;
var config = require('../../../config');

var title = "编辑画册";

exports.editGET = function (req, res, next) {
    var albumId = req.params._id;
    if (!stringTool.regMongoDBId(albumId)) {
        return next();
    }
    AlbumDAO.findAlbumById(albumId, function (err, album) {
        if (err) {
            return next(err);
        }
        if (album) {
            DrawingDAO.findDrawingsByAlbumId(album._id, function (err, drawings) {
                if (err) {
                    return next(err);
                }
                drawings.unshift({
//                    path: config.ALBUM_DEFAULT_PATH,
                    path: "default",
                    name: '默认封面'
                });
                var templateData = {
                    title: title,
                    drawings: drawings,
                    form: {}
                };
                var tags = [];
                for (var i = 0; i < album._tags.length; i++) {
                    tags.push(album._tags[i].name);
                }
                templateData.form.coverPath = album.coverPath;
                templateData.form.albumId = album._id;
                templateData.form.name = album.name;
                templateData.form.describe = album.describe;
                templateData.form.tags = tags.join(' ');
                res.render('album/edit/edit', templateData);
            });
        } else {
            next();
        }
    });
};


exports.editPOST = function (req, res, next) {
    var albumId = req.params._id;
    if (!stringTool.regMongoDBId(albumId)) {
        return next();
    }
    AlbumDAO.findAlbumById(albumId, function (err, album) {
        if (err) {
            return next(err);
        }
        if (album) {
            DrawingDAO.findDrawingsByAlbumId(album._id, function (err, drawings) {
                if (err) {
                    return next(err);
                }
                drawings.unshift({
//                    path: config.ALBUM_DEFAULT_PATH,
                    path: "default",
                    name: '默认封面'
                });

                var form = req.body.form;
                if (!form) {
                    return next();
                }
                var templateData = {
                    title: title,
                    drawings: drawings,
                    form: form
                };

                var name = sanitize(form.name).trim();
                name = sanitize(name).xss();
                try {
                    check(name).len(1, 20);
                    album.name = name;
                } catch (e) {
                    templateData.error = '请输入画册名,且画册名不超过20个字';
                    res.render('album/edit/edit', templateData);
                    return;
                }

                var coverPaths = [];
                for (var i in drawings) {
                    coverPaths[i] = drawings[i].path;
                }
                try {
                    check(form.coverPath).isIn(coverPaths);
                    album.coverPath = form.coverPath;
                } catch (e) {
                    templateData.error = '无效封面';
                    res.render('album/edit/edit', templateData);
                    return;
                }

                if (form.describe) {
                    var describe = sanitize(form.describe).trim();
                    describe = sanitize(describe).xss();
                    try {
                        check(describe).len(1, 200);
                        album.describe = describe;
                    } catch (e) {
                        templateData.error = "描述不超过200个字";
                        res.render('album/edit/edit', templateData);
                        return;
                    }
                } else {
                    album.describe = '';
                }

                var currentTags = album._tags;
                if (form.tags) {
                    var tags = sanitize(form.tags).trim();
                    var tags = tags.split(" ");
                    var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in tags
                    try {
                        if (tags.length > 5) {
                            throw new Error('标签数不大于5个');
                        }
                        for (var i in tags) {
                            var tag = sanitize(tags[i]).trim();
                            tag = sanitize(tag).xss();
                            if (tag.length > 15) {
                                throw new Error('标签字数大于15');
                            }
                            if (tag.length === 0 || tempObj[tag]) {
                                continue;
                            }
                            tempObj[tag] = true;
                            tempArray.push(tag);
                        }
                        album._doc._tags = tempArray;
                    } catch (e) {
                        templateData.error = "每个标签最多15个字，最多贴5个标签";
                        res.render('album/edit/edit', templateData);
                        return;
                    }
                } else {
                    album._tags = [];
                }

                var tagsSize = album._tags.length + currentTags.length;

                //no tags need deal
                if (!tagsSize) {
                    AlbumDAO.saveAlbum(album, function (err) {
                        if (err) {
                            return next(err);
                        }
                        templateData.success = "编辑成功";
                        res.render('album/edit/edit', templateData);
                    });
                    return;
                }

                //else
                //just decrease current tags' using count and increase new tags' using count simply,
                //then set new tags to db
                var newTagIds = [];

                function done(_id) {
                    if (_id) {
                        newTagIds.push(_id);
                    }
                    if (--tagsSize) {
                        return;
                    }
                    album._tags = newTagIds;
                    AlbumDAO.saveAlbum(album, function (err) {
                        if (err) {
                            return next(err);
                        }
                        templateData.success = "编辑成功";
                        res.render('album/edit/edit', templateData);
                    });
                }

                currentTags.forEach(function (tag) {
                    TagDAO.decUsingCountByName(tag.name, function (err) {
                        if (err) {
                            return next(err);
                        }
                        done();
                    });
                });

                album._tags.forEach(function (tagName) {
                    TagDAO.isExistByName(tagName, function (err, exist) {
                        if (err) {
                            return next(err);
                        }
                        if (exist) {
                            TagDAO.incUsingCountByName(tagName, function (err, tag) {
                                if (err) {
                                    return next(err);
                                }
                                done(tag._id);
                            });
                        } else {
                            var tagRaw = {
                                name: tagName,
                                _creator: req.session.user._id
                            };
                            TagDAO.newTag(tagRaw, function (err, tag) {
                                if (err) {
                                    return next(err);
                                }
                                done(tag._id);
                            });
                        }
                    });
                });
            });
        } else {
            next();
        }
    });
};