
var UserDAO = require('../../../DAO').UserDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var stringTool = require('../../../utilities').stringTool;
var config = require('../../../config');

var title = "画册"

exports.showOneGET = function (req, res, next) {
    var albumId = req.params._id;
    if (!stringTool.regMongoDBId(albumId)) {
        return next();
    }
    if (req.query.page) {
        DrawingDAO.findDrawingsByAlbumIdByPage(albumId, req.query.page, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
            if (err) {
                return next(err);
            }
            var templateData = {
                drawings: drawings
            }
            res.render('album/show_one/infinite_scroll_page', templateData);
        });
    } else {
        AlbumDAO.findAlbumById(albumId, function (err, album) {
            if (err) {
                return next(err);
            }
            if (album) {
                UserDAO.findUserById(album._creator, function(err, user){
                    if (err) {
                        return next(err);
                    }
                    DrawingDAO.findDrawingsByAlbumIdByPage(album._id, 1, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
                        if (err) {
                            return next(err);
                        }
                        album.formattedCreateTime = stringTool.formatTime2YMDHMS(album.createTime);
                        var templateData = {
                            title: title + album.name,
                            drawings: drawings,
                            album: album,
                            host: user
                        }
                        res.render('album/show_one/show_one', templateData);
                    });
                });
            } else {
                next();
            }
        });
    }
};