
var DrawingDAO = require('../../../DAO').DrawingDAO;
var config = require('../../../config');

var title = "热门作品";

exports.hotGET = function (req, res, next) {
    if (req.query.page) {
        DrawingDAO.findHotDrawingsByPage(req.query.page, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
            if (err) {
                return next(err);
            }
            var templateData = {
                drawings: drawings
            }
            res.render('drawing/hot/infinite_scroll_page', templateData);
        });
    } else {
        DrawingDAO.findHotDrawingsByPage(1, config.DRAWING_AMOUNT_PER_PAGE, function (err, drawings) {
            if (err) {
                return next(err);
            }
            var templateData = {
                title: title,
                drawings: drawings
            }
            res.render('drawing/hot/hot', templateData);
        });
    }
}