
var AlbumDAO = require('../../../DAO').AlbumDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var DrawingCommentDAO = require('../../../DAO').DrawingCommentDAO;
var DrawingCommentMessageDAO = require('../../../DAO').DrawingCommentMessageDAO;
var AtMessageDAO = require('../../../DAO').AtMessageDAO;
var CollectionDAO = require('../../../DAO').CollectionDAO;
var LikeDAO = require('../../../DAO').LikeDAO;
var UserDAO = require('../../../DAO').UserDAO;
var stringTool = require('../../../utilities').stringTool;
var qiniuService = require('../../../service').qiniuService;

exports.deletePOST = function (req, res, next) {
    var form = req.body.form;
    if (!form || !stringTool.regMongoDBId(form.drawing)) {
        return next();
    }
    DrawingDAO.findDrawingById(form.drawing, function (err, drawing) {
        if (err) {
            return next(err);
        }
        if (!drawing) {
            return next();
        }
        LikeDAO.removeLikesByDrawingId(form.drawing, function (err) {
            if (err) {
                return next(err);
            }
            CollectionDAO.removeCollectionsByDrawingId(form.drawing, function (err) {
                if (err) {
                    return next(err);
                }
                AtMessageDAO.removeMessagesByDrawingId(form.drawing, function (err) {
                    if (err) {
                        return next(err);
                    }
                    DrawingCommentMessageDAO.removeMessagesByDrawingId(form.drawing, function (err) {
                        if (err) {
                            return next(err);
                        }
                        DrawingCommentDAO.removeCommentsByDrawingId(form.drawing, function (err) {
                            if (err) {
                                return next(err);
                            }
                            DrawingDAO.removeDrawingById(form.drawing, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                AlbumDAO.findAlbumById(drawing._album._id, function (err, album) {
                                    if (err) {
                                        return next(err);
                                    }
                                    if (album.coverPath === drawing.path) {
                                        album.coverPath = 'default';
                                        AlbumDAO.saveAlbum(album, function (err) {
                                            if (err) {
                                                return next(err);
                                            }
                                            UserDAO.decPaintingCountById(req.session.user._id, function (err) {
                                                if (err) return next(err);
                                                qiniuService.deleteImage(drawing.path, function () {
                                                    res.redirect('/artist/' + req.session.user._id);
                                                }, next);
                                            });
                                        });
                                    } else {
                                        UserDAO.decPaintingCountById(req.session.user._id, function (err) {
                                            if (err) return next(err);
                                            qiniuService.deleteImage(drawing.path, function () {
                                                res.redirect('/artist/' + req.session.user._id);
                                            }, next);
                                        });
                                    }
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}