
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var config = require('../../../config');
var UserDAO = require('../../../DAO').UserDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var DrawingCommentDAO = require('../../../DAO').DrawingCommentDAO;
var DrawingCommentMessageDAO = require('../../../DAO').DrawingCommentMessageDAO;
var AtMessageDAO = require('../../../DAO').AtMessageDAO;
var stringTool = require('../../../utilities').stringTool;

var title = "作品"

exports.showOneGET = function (req, res, next) {
    var drawingId = req.params._id;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }
    DrawingDAO.findDrawingById(drawingId, function (err, drawing) {
        if (err) {
            return next(err);
        }
        if (!drawing) {
            return next();
        }
        drawing.formattedCreateTime = stringTool.formatTime2YMDHMS(drawing.createTime);
        DrawingCommentDAO.getCountByDrawingId(drawing._id, function (err, count) {
            if (err) {
                return next(err);
            }
            var pageAmount = 0, quotient = count / config.COMMENT_AMOUNT_PER_PAGE;
            if (quotient == Math.floor(quotient)) {
                pageAmount = quotient;
            } else {
                pageAmount = Math.floor(quotient) + 1;
            }
            var page = parseInt(req.query.page);
            page = page > 0 && page <= pageAmount ? page : 1;
            DrawingCommentDAO.findCommentsByDrawingId(drawing._id, page, config.COMMENT_AMOUNT_PER_PAGE, function (err, comments) {
                if (err) {
                    return next(err);
                }
                //no populate creator directly from finding drawing,because we want populate creator._tag,
                //so find user from UserDAO
                UserDAO.findUserById(drawing._creator, function (err, creator) {
                    if (err) {
                        return next(err);
                    }
                    var templateData = {
                        title: title + drawing.name,
                        drawing: drawing,
                        host: creator,
                        comments: comments,
                        currentPage: page,
                        pageAmount: pageAmount
                    }

                    comments.forEach(function (comment) {
                        comment.formattedCreateTime = stringTool.formatTime2YMDHMS(comment.createTime);
                    });

                    if (req.query.error === 'atLimit') {
                        templateData.error = "最多@10个用户";
                        res.render('drawing/show_one/show_one', templateData);
                        return;
                    } else if (req.query.error === 'atError') {
                        templateData.error = "@了无效用户";
                        res.render('drawing/show_one/show_one', templateData);
                        return;
                    } else if (req.query.error === 'content') {
                        templateData.error = '回复为1-200个字';
                        res.render('drawing/show_one/show_one', templateData);
                        return;
                    }

                    //simply add specified comment in the last of current page---for
                    // redirecting from message system or redirecting from this page post
                    var commentId = req.query.c;
                    if (commentId) {
                        if (!stringTool.regMongoDBId(commentId)) {
                            return next();
                        }
                        DrawingCommentDAO.findCommentById(commentId, function (err, comment) {
                            if (err) {
                                return next(err);
                            }
                            if (!comment) {
                                return next();
                            }
                            var alreadyIn = false;
                            for (var i in comments) {
                                if (comments[i].id === comment.id) {
                                    alreadyIn = true;
                                    break;
                                }
                            }
                            if (!alreadyIn) {
                                comment.formattedCreateTime = stringTool.formatTime2YMDHMS(comment.createTime);
                                comments.push(comment);
                            }
                            res.render('drawing/show_one/show_one', templateData);
                        });
                    } else {
                        res.render('drawing/show_one/show_one', templateData);
                    }
                });
            });
        });

    });
};

exports.showOnePOST = function (req, res, next) {
    var drawingId = req.params._id;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }
    DrawingDAO.findDrawingById(drawingId, function (err, drawing) {
        if (err) {
            return next(err);
        }
        if (!drawing) {
            return next();
        }
        var form = req.body.form;
        if (!form) {
            return next();
        }
        var commentRaw = {};
        var content = sanitize(form.content).trim();
        content = sanitize(content).xss();
        try {
            check(content).len(1, 200);
            commentRaw.content = content;
        } catch (e) {
            var page = req.query.page ? req.query.page : 1;
            res.redirect('drawing/' + drawing.id + '?page=' + page + '&error=content');
            return;
        }

        if (form.at) {
            var at = sanitize(form.at).trim();
            var at = at.split(' ');
            var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in at
            try {
                if (at.length > config.MAX_AT_AMOUNT) {
                    throw new Error('最多@10个用户');
                }
                for (var i in at) {
                    var atItem = sanitize(at[i]).trim();
                    atItem = sanitize(atItem).xss();
                    if (atItem.length === 0 || tempObj[atItem]) {
                        continue;
                    }
                    tempObj[atItem] = true;
                    tempArray.push(atItem);
                }
                commentRaw._at = tempArray;
            } catch (e) {
                var page = req.query.page ? req.query.page : 1;
                res.redirect('drawing/' + drawing.id + '?page=' + page + '&error=atLimit');
                return;
            }
        } else {
            commentRaw._at = [];
        }

        function responseNoAt() {
            commentRaw._creator = req.session.user._id;
            commentRaw._drawing = drawing._id;
            DrawingCommentDAO.newComment(commentRaw, function (err, newComment) {
                if (err) {
                    return next(err);
                }
                // self's comment,don't create message
                if (drawing._creator.toString() === req.session.user._id) {
                    DrawingDAO.incCommentCountById(drawing._id, function (err) {
                        if (err) {
                            return next(err);
                        }
                        var page = req.query.page ? req.query.page : 1;
                        res.redirect('drawing/' + drawing.id + '?page=' + page + '&c=' + newComment.id);
                    });
                } else {
                    var drawingCommentMessage = {};
                    drawingCommentMessage._drawing = drawing._id;
                    drawingCommentMessage._drawingComment = newComment._id;
                    drawingCommentMessage._from = req.session.user._id;
                    drawingCommentMessage._to = drawing._creator;
                    DrawingCommentMessageDAO.newMessage(drawingCommentMessage, function (err) {
                        if (err) {
                            return next(err);
                        }
                        DrawingDAO.incCommentCountById(drawing._id, function (err) {
                            if (err) {
                                return next(err);
                            }
                            var page = req.query.page ? req.query.page : 1;
                            res.redirect('drawing/' + drawing.id + '?page=' + page + '&c=' + newComment.id);
                        });
                    });
                }
            });
        }

        function responseAt() {
            commentRaw._creator = req.session.user._id;
            commentRaw._drawing = drawing._id;
            DrawingCommentDAO.newComment(commentRaw, function (err, newComment) {
                if (err) {
                    return next(err);
                }
                // self's comment,don't create message
                if (drawing._creator.toString() === req.session.user._id) {
                    var atSize = newComment._at.length;

                    function atMessageDone() {
                        if (--atSize) {
                            return;
                        }
                        DrawingDAO.incCommentCountById(drawing._id, function (err) {
                            if (err) {
                                return next(err);
                            }
                            var page = req.query.page ? req.query.page : 1;
                            res.redirect('drawing/' + drawing.id + '?page=' + page + '&c=' + newComment.id);
                        });
                    }

                    newComment._at.forEach(function (at) {
                        var atMessage = {};
                        atMessage._drawingComment = newComment._id;
                        atMessage._drawing = drawing._id;
                        atMessage._from = req.session.user._id;
                        atMessage._to = at;
                        AtMessageDAO.newMessage(atMessage, function (err) {
                            if (err) {
                                return next(err);
                            }
                            atMessageDone();
                        });
                    });
                } else {
                    var drawingCommentMessage = {};
                    drawingCommentMessage._drawing = drawing._id;
                    drawingCommentMessage._drawingComment = newComment._id;
                    drawingCommentMessage._from = req.session.user._id;
                    drawingCommentMessage._to = drawing._creator;
                    DrawingCommentMessageDAO.newMessage(drawingCommentMessage, function (err) {
                        if (err) {
                            return next(err);
                        }
                        var atSize = newComment._at.length;

                        function atMessageDone() {
                            if (--atSize) {
                                return;
                            }
                            DrawingDAO.incCommentCountById(drawing._id, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                var page = req.query.page ? req.query.page : 1;
                                res.redirect('drawing/' + drawing.id + '?page=' + page + '&c=' + newComment.id);
                            });
                        }

                        newComment._at.forEach(function (at) {
                            var atMessage = {};
                            atMessage._drawingComment = newComment._id;
                            atMessage._drawing = drawing._id;
                            atMessage._from = req.session.user._id;
                            atMessage._to = at;
                            AtMessageDAO.newMessage(atMessage, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                atMessageDone();
                            });
                        });
                    });
                }
            });
        }

        var atSize = commentRaw._at.length;
        if (!atSize) {
            responseNoAt();
            return;
        }

        var atIds = [];

        function atDone(_id) {
            if (_id) {
                atIds.push(_id);
            }
            if (--atSize) {
                return;
            }
            if (atIds.length != commentRaw._at.length) {
                var page = req.query.page ? req.query.page : 1;
                res.redirect('drawing/' + drawing.id + '?page=' + page + '&error=atError');
            } else {
                commentRaw._at = atIds;
                responseAt();
            }
        }

        for (var i in commentRaw._at) {
            commentRaw._at[i] = commentRaw._at[i].slice(1);
            UserDAO.findUserByName(commentRaw._at[i], function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    atDone(user._id);
                } else {
                    atDone();
                }
            });
        }
    });
}