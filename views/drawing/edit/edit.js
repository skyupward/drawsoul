
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var TagDAO = require('../../../DAO').TagDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var stringTool = require('../../../utilities').stringTool;

var title = '编辑作品';

exports.editGET = function (req, res, next) {
    var drawingId = req.params._id;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }
    AlbumDAO.findAlbumsByUserId(req.session.user._id, function (err, albums) {
        if (err) {
            return next(err);
        }
        var templateData = {
            title: title,
            albums: albums,
            form: {}
        };
        DrawingDAO.findDrawingById(drawingId, function (err, drawing) {
            if (err) {
                return next(err);
            }
            if (drawing) {
                var tags = [];
                for (var i = 0; i < drawing._tags.length; i++) {
                    tags.push(drawing._tags[i].name);
                }
                templateData.form.drawingId = drawing._id;
                templateData.form.name = drawing.name;
                templateData.form.album = drawing._album.id;
                templateData.form.describe = drawing.describe;
                templateData.form.tags = tags.join(' ');
                res.render('drawing/edit/edit', templateData);
            } else {
                next();
            }
        });
    });
};

exports.editPOST = function (req, res, next) {
    var drawingId = req.params._id;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }
    AlbumDAO.findAlbumsByUserId(req.session.user._id, function (err, albums) {
            if (err) {
                return next(err);
            }
            DrawingDAO.findDrawingById(drawingId, function (err, drawing) {
                if (err) {
                    return next(err);
                }
                if (drawing) {
                    var form = req.body.form;
                    if (!form) {
                        return next();
                    }
                    if (!form) {
                        return next();
                    }
                    var templateData = {
                        title: title,
                        albums: albums,
                        form: form
                    };

                    var name = sanitize(form.name).trim();
                    name = sanitize(name).xss();
                    try {
                        check(name).len(1, 20);
                        drawing.name = name;
                    } catch (e) {
                        templateData.error = '请输入画名,且画名不超过20个字';
                        res.render('drawing/edit/edit', templateData);
                        return;
                    }

                    var albumIds = [];
                    for (var i in albums) {
                        albumIds[i] = albums[i].id;
                    }
                    try {
                        check(form.album).isIn(albumIds);
                    } catch (e) {
                        templateData.error = '画册非法';
                        res.render('drawing/edit/edit', templateData);
                        return;
                    }

                    if (form.describe) {
                        var describe = sanitize(form.describe).trim();
                        describe = sanitize(describe).xss();
                        try {
                            check(describe).len(1, 200);
                            drawing.describe = describe;
                        } catch (e) {
                            templateData.error = "描述不超过200个字";
                            res.render('drawing/edit/edit', templateData);
                            return;
                        }
                    } else {
                        drawing.describe = '';
                    }

                    var currentTags = drawing._tags;
                    if (form.tags) {
                        var tags = sanitize(form.tags).trim();
                        var tags = tags.split(" ");
                        var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in tags
                        try {
                            if (tags.length > 5) {
                                throw new Error('标签数不大于5个');
                            }
                            for (var i in tags) {
                                var tag = sanitize(tags[i]).trim();
                                tag = sanitize(tag).xss();
                                if (tag.length > 15) {
                                    throw new Error('标签字数大于15');
                                }
                                if (tag.length === 0 || tempObj[tag]) {
                                    continue;
                                }
                                tempObj[tag] = true;
                                tempArray.push(tag);
                            }
                            drawing._doc._tags = tempArray;
                        } catch (e) {
                            templateData.error = "每个标签最多15个字，最多贴5个标签";
                            res.render('drawing/edit/edit', templateData);
                            return;
                        }
                    } else {
                        drawing._tags = [];
                    }

                    var tagsSize = drawing._tags.length + currentTags.length;

                    //no tags need deal
                    if (!tagsSize) {
                        //current drawing is current drawing's album's cover & move to other album
                        if (drawing._album.coverPath === drawing.path && drawing._album.id != form.album) {
                            drawing._album.coverPath = 'default';
                            AlbumDAO.saveAlbum(drawing._album, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                drawing._album = form.album;
                                DrawingDAO.saveDrawing(drawing, function (err) {
                                    if (err) {
                                        return next(err);
                                    }
                                    templateData.success = "编辑成功";
                                    res.render('drawing/edit/edit', templateData);
                                });

                            });
                        }else{
                            drawing._album = form.album;
                            DrawingDAO.saveDrawing(drawing, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                templateData.success = "编辑成功";
                                res.render('drawing/edit/edit', templateData);
                            });
                        }
                        return;
                    }

                    //else
                    //just decrease current tags' using count and increase new tags' using count simply,
                    //then set new tags to db
                    var newTagIds = [];

                    function done(_id) {
                        if (_id) {
                            newTagIds.push(_id);
                        }
                        if (--tagsSize) {
                            return;
                        }
                        drawing._tags = newTagIds;
                        //current drawing is current drawing's album's cover & move to other album
                        if (drawing._album.coverPath === drawing.path && drawing._album.id != form.album) {
                            drawing._album.coverPath = 'default';
                            AlbumDAO.saveAlbum(drawing._album, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                drawing._album = form.album;
                                DrawingDAO.saveDrawing(drawing, function (err) {
                                    if (err) {
                                        return next(err);
                                    }
                                    templateData.success = "编辑成功";
                                    res.render('drawing/edit/edit', templateData);
                                });

                            });
                        }else{
                            drawing._album = form.album;
                            DrawingDAO.saveDrawing(drawing, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                templateData.success = "编辑成功";
                                res.render('drawing/edit/edit', templateData);
                            });
                        }
                    }

                    currentTags.forEach(function (tag) {
                        TagDAO.decUsingCountByName(tag.name, function (err) {
                            if (err) {
                                return next(err);
                            }
                            done();
                        });
                    });

                    drawing._tags.forEach(function (tagName) {
                        TagDAO.isExistByName(tagName, function (err, exist) {
                            if (err) {
                                return next(err);
                            }
                            if (exist) {
                                TagDAO.incUsingCountByName(tagName, function (err, tag) {
                                    if (err) {
                                        return next(err);
                                    }
                                    done(tag._id);
                                });
                            } else {
                                var tagRaw = {
                                    name: tagName,
                                    _creator: req.session.user._id
                                };
                                TagDAO.newTag(tagRaw, function (err, tag) {
                                    if (err) {
                                        return next(err);
                                    }
                                    done(tag._id);
                                });
                            }
                        });
                    });
                } else {
                    next();
                }
            });
        }
    );
};