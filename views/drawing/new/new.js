
var fs = require('fs');
var config = require('../../../config')
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var AlbumDAO = require('../../../DAO').AlbumDAO;
var TagDAO = require('../../../DAO').TagDAO;
var DrawingDAO = require('../../../DAO').DrawingDAO;
var UserDAO = require('../../../DAO').UserDAO;
var qiniuService = require('../../../service').qiniuService;

var title = "新的一幅画";

exports.newGET = function (req, res, next) {
    var user = req.session.user;
    AlbumDAO.findAlbumsByUserId(user._id, function (err, albums) {
        if (err) {
            return next(err);
        }
        var upToken = qiniuService.imageUpToken();
        var fileKey = user._id + '/' + new Date().getTime();
        var templateData = {
            title: title,
            albums: albums,
            fileKey: fileKey,
            upToken: upToken
        };
        var ret = req.query.upload_ret;
        if (ret) {
            ret = qiniuService.restoreReturnBody(ret);
            try {
                ret = JSON.parse(ret);
            } catch (e) {
                return next(e);
            }
            templateData.form = ret;
            var drawingRaw = {};
            var name = sanitize(ret.name).trim();
            name = sanitize(name).xss();
            try {
                check(name).len(1, 20);
                drawingRaw.name = name;
            } catch (e) {
                qiniuService.deleteImage(ret.key, function () {
                    templateData.error = '请输入画名,且画名不超过20个字';
                    res.render('drawing/new/new', templateData);
                }, next);
                return;
            }

            if (ret.size === 0) {
                qiniuService.deleteImage(ret.key, function () {
                    templateData.error = '请选择图片文件';
                    res.render('drawing/new/new', templateData);
                }, next);
                return;
            }

            try {
                check(ret.type).isIn(config.IMG_TYPE);
            } catch (e) {
                qiniuService.deleteImage(ret.key, function () {
                    templateData.error = '只能上传png, jpeg, jpg, gif文件';
                    res.render('drawing/new/new', templateData);
                }, next);
                return;
            }

            if (ret.size > config.DRAWING_SIZE_LIMIT) {
                qiniuService.deleteImage(ret.key, function () {
                    templateData.error = '文件不能大于' + config.DRAWING_SIZE_LIMIT / 1024 / 1024 + 'MB';
                    res.render('drawing/new/new', templateData);
                }, next);
                return;
            }

            var albumIds = [];
            for (var i in albums) {
                albumIds[i] = albums[i].id;
            }
            try {
                check(ret.album).isIn(albumIds);
                drawingRaw._album = ret.album;
            } catch (e) {
                qiniuService.deleteImage(ret.key, function () {
                    templateData.error = '画册非法';
                    res.render('drawing/new/new', templateData);
                }, next);
                return;
            }

            if (ret.describe) {
                var describe = sanitize(ret.describe).trim();
                describe = sanitize(describe).xss();
                try {
                    check(describe).len(1, 200);
                    drawingRaw.describe = describe;
                } catch (e) {
                    qiniuService.deleteImage(ret.key, function () {
                        templateData.error = '描述不超过200个字';
                        res.render('drawing/new/new', templateData);
                    }, next);
                    return;
                }
            }

            if (ret.tags) {
                var tags = sanitize(ret.tags).trim();
                var tags = tags.split(" ");
                var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in tags
                try {
                    if (tags.length > 5) {
                        throw new Error('标签数不大于5个');
                    }
                    for (var i in tags) {
                        var tag = sanitize(tags[i]).trim();
                        tag = sanitize(tag).xss();
                        if (tag.length > 15) {
                            throw new Error('标签字数大于15');
                        }
                        if (tag.length === 0 || tempObj[tag]) {
                            continue;
                        }
                        console.log(tag.length);
                        tempObj[tag] = true;
                        tempArray.push(tag);
                    }
                    drawingRaw._tags = tempArray;
                } catch (e) {
                    qiniuService.deleteImage(ret.key, function () {
                        templateData.error = '每个标签最多15个字，最多贴5个标签';
                        res.render('drawing/new/new', templateData);
                    }, next);
                    return;
                }
            }

            function response() {
                drawingRaw.path = ret.key;
                drawingRaw._creator = user._id;
                DrawingDAO.newDrawing(drawingRaw, function (err) {
                    if (err) {
                        return next(err);
                    }
                    UserDAO.incPaintingCountById(user._id, function (err) {
                        if (err) return next(err);
                        templateData.drawing = ret.key;
                        templateData.success = '一幅非常棒的画已经发布';
                        res.render('drawing/new/new', templateData);
                    });
                });
            }

            //no tags need deal
            if (!drawingRaw._tags) {
                response();
                return;
            }

            //else
            //just increase new tags' using count simply,
            //then set new tags to db
            var tagsSize = drawingRaw._tags.length;
            var newTagIds = [];

            function done(_id) {
                newTagIds.push(_id);
                if (--tagsSize) {
                    return;
                }
                drawingRaw._tags = newTagIds;
                response();
            }

            drawingRaw._tags.forEach(function (tagName) {
                TagDAO.isExistByName(tagName, function (err, exist) {
                    if (err) {
                        return next(err);
                    }
                    if (exist) {
                        TagDAO.incUsingCountByName(tagName, function (err, tag) {
                            if (err) {
                                return next(err);
                            }
                            done(tag._id);
                        });
                    } else {
                        var tagRaw = {
                            name: tagName,
                            _creator: req.session.user._id
                        };
                        TagDAO.newTag(tagRaw, function (err, tag) {
                            if (err) {
                                return next(err);
                            }
                            done(tag._id);
                        });
                    }
                });
            });
        } else {
            templateData.form = "";
            if (albums.length) {
                return res.render('drawing/new/new', templateData);
            }
            var albumRaw = {
                name: '默认画册',
                describe: '默认画册',
                _creator: user._id,
                coverPath: 'default'//use qiniu
//                coverPath: config.ALBUM_DEFAULT_PATH
            };
            AlbumDAO.newAlbum(albumRaw, function (err, album) {
                if (err) {
                    return next(err);
                }
                albums.push(album);
                res.render('drawing/new/new', templateData);
            });
        }
    });
};

//post no used if use qiniu
exports.newPOST = function (req, res, next) {
//    AlbumDAO.findAlbumsByUserId(req.session.user._id, function (err, albums) {
//            if (err) {
//                return next(err);
//            }
//            var form = req.body.form;
//            if (!form) {
//                return next();
//            }
//            var drawing = req.files.form.drawing;
//            var templateData = {
//                title: title,
//                albums: albums,
//                form: form
//            };

//            var drawingRaw = {};
//            var name = sanitize(form.name).trim();
//            name = sanitize(name).xss();
//            try {
//                check(name).len(1, 20);
//                drawingRaw.name = name;
//            } catch (e) {
//                templateData.error = '请输入画名,且画名不超过20个字';
//                res.render('drawing/new/new', templateData);
//                return;
//            }
//
//            if (drawing.size === 0) {
//                templateData.error = '请选择图片文件';
//                res.render('drawing/new/new', templateData);
//                return;
//            }
//
//            try {
//                check(drawing.type).isIn(config.IMG_TYPE);
//            } catch (e) {
//                templateData.error = '只能上传png, jpeg, jpg, gif文件';
//                res.render('drawing/new/new', templateData);
//                return;
//            }
//
//            if (drawing.size > config.DRAWING_SIZE_LIMIT) {
//                templateData.error = '文件不能大于' + config.DRAWING_SIZE_LIMIT / 1024 / 1024 + 'MB';
//                res.render('drawing/new/new', templateData);
//                return;
//            }
//
//            var albumIds = [];
//            for (var i in albums) {
//                albumIds[i] = albums[i].id;
//            }
//            try {
//                check(form.album).isIn(albumIds);
//                drawingRaw._album = form.album;
//            } catch (e) {
//                templateData.error = '画册非法';
//                res.render('drawing/new/new', templateData);
//                return;
//            }
//
//            if (form.describe) {
//                var describe = sanitize(form.describe).trim();
//                describe = sanitize(describe).xss();
//                try {
//                    check(describe).len(1, 200);
//                    drawingRaw.describe = describe;
//                } catch (e) {
//                    templateData.error = "描述不超过200个字";
//                    res.render('drawing/new/new', templateData);
//                    return;
//                }
//            }
//
//            if (form.tags) {
//                var tags = sanitize(form.tags).trim();
//                var tags = tags.split(" ");
//                var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in tags
//                try {
//                    if (tags.length > 5) {
//                        throw new Error('标签数不大于5个');
//                    }
//                    for (var i in tags) {
//                        var tag = sanitize(tags[i]).trim();
//                        tag = sanitize(tag).xss();
//                        if (tag.length > 15) {
//                            throw new Error('标签字数大于15');
//                        }
//                        if (tag.length === 0 || tempObj[tag]) {
//                            continue;
//                        }
//                        tempObj[tag] = true;
//                        tempArray.push(tag);
//                    }
//                    drawingRaw._tags = tempArray;
//                } catch (e) {
//                    templateData.error = "每个标签最多15个字，最多贴5个标签";
//                    res.render('drawing/new/new', templateData);
//                    return;
//                }
//            }
//
//            function mkdirAndResponse() {
//
//                var tmp_path = drawing.path;
//                var target_dir = 'public/images/user/drawing/' + req.session.user._id;
//                var target_path = target_dir + '/' + new Date().getTime() + '_' + drawing.name;
//
//                function moveFileAndResponse() {
//                    fs.rename(tmp_path, target_path, function (err) {
//                        if (err) return next(err);
//                        fs.unlink(tmp_path, function () {
//                            if (err) return next(err);
//                            drawingRaw.path = target_path.slice(6);
//                            drawingRaw._creator = req.session.user._id;
//                            DrawingDAO.newDrawing(drawingRaw, function (err) {
//                                if (err) {
//                                    return next(err);
//                                }
//                                templateData.form.drawing = drawingRaw.path;
//                                UserDAO.incPaintingCountById(req.session.user._id, function (err) {
//                                    if (err) return next(err);
//                                    templateData.success = "一幅非常棒的画已经发布";
//                                    res.render('drawing/new/new', templateData);
//                                });
//                            });
//                        });
//                    });
//                }
//
//                fs.exists(target_dir, function (exists) {
//                    if (exists) {
//                        moveFileAndResponse();
//                    } else {
//                        fs.mkdir(target_dir, function (err) {
//                            if (err) return next(err);
//                            moveFileAndResponse();
//                        });
//                    }
//                });
//            }
//
//            //no tags need deal
//            if (!drawingRaw._tags) {
//                mkdirAndResponse();
//                return;
//            }
//
//            //else
//            //just increase new tags' using count simply,
//            //then set new tags to db
//            var tagsSize = drawingRaw._tags.length;
//            var newTagIds = [];
//
//            function done(_id) {
//                newTagIds.push(_id);
//                if (--tagsSize) {
//                    return;
//                }
//                drawingRaw._tags = newTagIds;
//                mkdirAndResponse();
//            }
//
//            drawingRaw._tags.forEach(function (tagName) {
//                TagDAO.isExistByName(tagName, function (err, exist) {
//                    if (err) {
//                        return next(err);
//                    }
//                    if (exist) {
//                        TagDAO.incUsingCountByName(tagName, function (err, tag) {
//                            if (err) {
//                                return next(err);
//                            }
//                            done(tag._id);
//                        });
//                    } else {
//                        var tagRaw = {
//                            name: tagName,
//                            _creator: req.session.user._id
//                        };
//                        TagDAO.newTag(tagRaw, function (err, tag) {
//                            if (err) {
//                                return next(err);
//                            }
//                            done(tag._id);
//                        });
//                    }
//                });
//            });
//        }
//    );
};