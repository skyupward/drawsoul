
exports.chatListGET = require('./chat_list').chatListGET;
exports.chatListPOST = require('./chat_list').chatListPOST;
exports.sendChatGET = require('./send_chat').sendChatGET;
exports.sendChatPOST = require('./send_chat').sendChatPOST;