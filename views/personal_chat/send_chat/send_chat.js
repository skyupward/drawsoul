
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var stringTool = require('../../../utilities').stringTool;
var UserDAO = require('../../../DAO').UserDAO;
var config = require('../../../config');
var PersonalChatDAO = require('../../../DAO').PersonalChatDAO;
var PersonalChatMessageDAO = require('../../../DAO').PersonalChatMessageDAO;

var title = "发送私信";

exports.sendChatGET = function (req, res, next) {
    var id = req.params._id;
    if (!stringTool.regMongoDBId(id)) {
        return next();
    }
    UserDAO.findUserById(id, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            var templateData = {
                title: title,
                to: user
            };
            if(req.query.error === 'limit'){
                templateData.error = "私信不超过200个字";
            }else if(req.query.success === 'ok'){
                templateData.success = "发送成功";
            }
            res.render('personal_chat/send_chat/send_chat', templateData);
        } else {
            next();
        }
    });
};

exports.sendChatPOST = function (req, res, next) {
    var talker1 = req.params._id, talker2 = req.session.user._id,
        form = req.body.form;
    if (!stringTool.regMongoDBId(talker1) || !form) {
        return next();
    }
    UserDAO.findUserById(talker1, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            var content = sanitize(form.content).trim();
            content = sanitize(content).xss();
            try {
                check(content).len(1, 200);
            } catch (e) {
                res.redirect('chat/send/' + user.id  + '?error=limit');
                return;
            }
            var recordRaw = {
                _from: talker2,
                _to: talker1,
                content: content
            };
            PersonalChatDAO.newChatRecord(recordRaw, function (err, chatRecord) {
                if (err) {
                    return next(err);
                }
                PersonalChatMessageDAO.findMessageByTalkers(talker2, talker1, function (err, message) {
                    if (err) {
                        return next(err);
                    }
                    if (message) {
                        message.read = false;
                        message.createTime = new Date();
                        message._personalChat = chatRecord._id;
                        PersonalChatMessageDAO.saveMessage(message, function (err) {
                            if (err) {
                                return next(err);
                            }
                            res.redirect('chat/send/' + user.id + '?success=ok');
                        });
                    } else {
                        var messageRaw = {
                            _from: talker2,
                            _to: talker1,
                            _personalChat: chatRecord._id
                        };
                        PersonalChatMessageDAO.newMessage(messageRaw, function (err) {
                            if (err) {
                                return next(err);
                            }
                            res.redirect('chat/send/' + user.id + '?success=ok');
                        });
                    }
                });
            });
        } else {
            next();
        }
    });
};