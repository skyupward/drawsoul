
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var stringTool = require('../../../utilities').stringTool;
var config = require('../../../config');
var UserDAO = require('../../../DAO').UserDAO;
var PersonalChatDAO = require('../../../DAO').PersonalChatDAO;
var PersonalChatMessageDAO = require('../../../DAO').PersonalChatMessageDAO;

var title = "私信";

exports.chatListGET = function (req, res, next) {
    var talker1 = req.params._id, talker2 = req.session.user._id;
    if (!stringTool.regMongoDBId(talker1)) {
        return next();
    }
    UserDAO.findUserById(talker1, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            PersonalChatDAO.getCountIn30Days(function (err, count) {
                if (err) {
                    return next(err);
                }
                var pageAmount = 0, quotient = count / config.CHAT_AMOUNT_PER_PAGE;
                if (quotient == Math.floor(quotient)) {
                    pageAmount = quotient;
                } else {
                    pageAmount = Math.floor(quotient) + 1;
                }
                var page = parseInt(req.query.page);
                page = page > 0 && page <= pageAmount ? page : 1;
                PersonalChatDAO.findChatRecordsIn30Days(talker1, talker2, page, config.CHAT_AMOUNT_PER_PAGE, function (err, chatRecords) {
                    chatRecords.forEach(function (record) {
                        record.formattedCreateTime = stringTool.formatTime2YMDHMS(record.createTime);
                    });
                    if (err) {
                        return next(err);
                    }
                    var templateData = {
                        title: title,
                        chatRecords: chatRecords,
                        currentPage: page,
                        pageAmount: pageAmount
                    };
                    if(req.query.error === "limit"){
                        templateData.error = "私信不超过200个字";
                        res.render('personal_chat/chat_list/chat_list', templateData);
                        return;
                    }

                    //send a new personal chat
                    if(req.query.c){
                        if (!stringTool.regMongoDBId(req.query.c)) {
                            return next();
                        }
                        PersonalChatDAO.findChatRecordById(req.query.c, function (err, record) {
                            if (err) {
                                return next(err);
                            }
                            if (!record) {
                                return next();
                            }
                            var alreadyIn = false;
                            for (var i in chatRecords) {
                                if (chatRecords[i].id === record.id) {
                                    alreadyIn = true;
                                    break;
                                }
                            }
                            if (!alreadyIn) {
                                record.formattedCreateTime = stringTool.formatTime2YMDHMS(record.createTime);
                                chatRecords.unshift(record);
                            }
                            res.render('personal_chat/chat_list/chat_list', templateData);
                        });
                    }else{
                        res.render('personal_chat/chat_list/chat_list', templateData);
                    }
                });
            });
        } else {
            next();
        }
    });
};

exports.chatListPOST = function (req, res, next) {
    var talker1 = req.params._id, talker2 = req.session.user._id,
        form = req.body.form;
    if (!stringTool.regMongoDBId(talker1) || !form) {
        return next();
    }
    UserDAO.findUserById(talker1, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            var content = sanitize(form.content).trim();
            content = sanitize(content).xss();
            try {
                check(content).len(1, 200);
                var recordRaw = {
                    _from: talker2,
                    _to: talker1,
                    content: content
                };
                PersonalChatDAO.newChatRecord(recordRaw, function (err, chatRecord) {
                    if (err) {
                        return next(err);
                    }
                    PersonalChatMessageDAO.findMessageByTalkers(talker2, talker1, function (err, message) {
                        if (err) {
                            return next(err);
                        }
                        if (message) {
                            message.read = false;
                            message.createTime = new Date();
                            message._personalChat = chatRecord._id;
                            PersonalChatMessageDAO.saveMessage(message, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                var page = req.query.page ? req.query.page : 1;
                                res.redirect('chat/' + user.id + '?page=' + page + '&c=' + chatRecord.id);

                            });
                        } else {
                            var messageRaw = {
                                _from: talker2,
                                _to: talker1,
                                _personalChat: chatRecord._id
                            };
                            PersonalChatMessageDAO.newMessage(messageRaw, function (err) {
                                if (err) {
                                    return next(err);
                                }
                                var page = req.query.page ? req.query.page : 1;
                                res.redirect('chat/' + user.id + '?page=' + page + '&c=' + chatRecord.id);
                            });
                        }
                    });
                });
            } catch (e) {
                var page = req.query.page ? req.query.page : 1;
                res.redirect('chat/' + user.id + '?page=' + page + '&error=limit');
            }
        } else {
            next();
        }
    });
};