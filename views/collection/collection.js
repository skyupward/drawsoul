
var CollectionDAO = require('../../DAO').CollectionDAO;
var DrawingDAO = require('../../DAO').DrawingDAO;
var stringTool = require('../../utilities').stringTool;

exports.collectionGET = function (req, res, next) {

    var drawingId = req.query.drawing;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }

    CollectionDAO.getCountByDrawingId(drawingId, function (err, count) {
        if (err) {
            return next(err);
        }
        CollectionDAO.findCollectionByUserIdByDrawingId(req.session.user._id, drawingId, function (err, collection) {
            if (err) {
                return next(err);
            }
            var data = {count: count};
            if (collection) {
                data.collection = true;
                res.json(data);
            } else {
                data.collection = false;
                res.json(data);
            }
        });
    });
};
exports.collectionPOST = function (req, res, next) {

    var drawingId = req.body.drawing;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }

    CollectionDAO.findCollectionByUserIdByDrawingId(req.session.user._id, drawingId, function (err, collection) {
        if (err) {
            return next(err);
        }
        var action = req.body.action;
        if(action === 'collection' && !collection){
            var collectionRaw = {
                _drawing: drawingId,
                _creator: req.session.user._id
            };
            CollectionDAO.newCollection(collectionRaw, function (err) {
                if (err) {
                    return next(err);
                }
                DrawingDAO.incCollectCountById(drawingId, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.json({collection: true});
                });
            });
        }else if(action === 'cancel' && collection){
            CollectionDAO.cancelCollectionByUserIdByDrawingId(req.session.user._id, drawingId, function (err) {
                if (err) {
                    return next(err);
                }
                DrawingDAO.decCollectCountById(drawingId, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.json({collection: false});
                });
            });
        }else if(action === 'collection' && collection){
            res.json({collection: true});
        }else if(action === 'cancel' && !collection){
            res.json({collection: false});
        }else{
            next();
        }
    });
};