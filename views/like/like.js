
var LikeDAO = require('../../DAO').LikeDAO;
var DrawingDAO = require('../../DAO').DrawingDAO;
var stringTool = require('../../utilities').stringTool;

exports.likeGET = function (req, res, next) {

    var drawingId = req.query.drawing;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }

    LikeDAO.getCountByDrawingId(drawingId, function (err, count) {
        if (err) {
            return next(err);
        }
        LikeDAO.findLikeByUserIdByDrawingId(req.session.user._id, drawingId, function (err, like) {
            if (err) {
                return next(err);
            }
            var data = {count: count};
            if (like) {
                data.like = true;
                res.json(data);
            } else {
                data.like = false;
                res.json(data);
            }
        });
    });
};

exports.likePOST = function (req, res, next) {

    var drawingId = req.body.drawing;
    if (!stringTool.regMongoDBId(drawingId)) {
        return next();
    }

    LikeDAO.findLikeByUserIdByDrawingId(req.session.user._id, drawingId, function (err, like) {
        if (err) {
            return next(err);
        }
        var action = req.body.action;
        if(action === 'like' && !like){
            var likeRaw = {
                _drawing: drawingId,
                _creator: req.session.user._id
            };
            LikeDAO.newLike(likeRaw, function (err) {
                if (err) {
                    return next(err);
                }
                DrawingDAO.incLikeCountById(drawingId, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.json({like: true});
                });
            });
        }else if(action === 'cancel' && like){
            LikeDAO.cancelLikeByUserIdByDrawingId(req.session.user._id, drawingId, function (err) {
                if (err) {
                    return next(err);
                }
                DrawingDAO.decLikeCountById(drawingId, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.json({like: false});
                });
            });
        }else if(action === 'like' && like){
            res.json({like: true});
        }else if(action === 'cancel' && !like){
            res.json({like: false});
        }else{
            next();
        }
    });
};