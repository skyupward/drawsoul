
var hashTool = require('../../../utilities').hashTool;
var stringTool = require('../../../utilities').stringTool;
var UserDAO = require('../../../DAO').UserDAO;
var config = require('../../../config');
exports.activateAccountGET = function (req, res, next) {
    if (!stringTool.regRegisterToken(req.params.token)) {
        next();
        return;
    }
    var id = req.params.token.slice(-24);
    var token = req.params.token.slice(0, 64);
    UserDAO.findUserById(id, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            if (!user.activated && (token === hashTool.hmacSha256Hex(user.id, config.REGISTER_SECRET_KEY))) {
                user.activated = true;
                UserDAO.saveUser(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    req.session.regenerate(function (err) {
                        if (err) {
                            return next(err);
                        }
                        req.session.user = user;
                        res.redirect('/set/basic');
                    });
                });
            } else {
                res.redirect('/login');
            }
        } else {
            next();//let it go, and it will raise a 404
        }
    });

};