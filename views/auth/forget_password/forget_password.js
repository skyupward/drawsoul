
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var mailService = require('../../../service').mailService;
var utilities = require('../../../utilities');
var hashTool = utilities.hashTool;
var randomTool = utilities.randomTool;
var UserDAO = require('../../../DAO').UserDAO;

var title = '忘记密码';

exports.forgetPasswordGET = function (req, res) {
    var templateData = {
        title: title
    };
    if (req.query.error === 'email') {
        templateData.error = '请输入正确的邮箱';
    } else if (req.query.error === 'user') {
        templateData.error = '用户不存在';
    } else if (req.query.success === 'ok') {
        templateData.success = '一封重置邮件已经发到您的邮箱';
    }
    res.render('auth/forget_password/forget_password', templateData);
};

exports.forgetPasswordPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var email = sanitize(form.email).trim().toLowerCase();
    try {
        check(email).isEmail();
    } catch (e) {
        res.redirect('/forget/password?error=email');
        return;
    }
    UserDAO.findUserByEmail(email, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                user.resetPasswordToken = hashTool.hashSha256Hex(randomTool.randomString(18));
                var now = new Date();
                user.resetPasswordExpires = now.setDate(now.getDate() + 1);
                UserDAO.saveUser(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    mailService.sendResetPasswordEmail(email, user.name, user.resetPasswordToken + user.id, function (err) {
                        if (err) {
                            return next(err);
                        }
                        res.redirect('/forget/password?success=ok');
                    });
                });
            }
            else {
                res.redirect('/forget/password?error=user');
            }
        }
    );
};