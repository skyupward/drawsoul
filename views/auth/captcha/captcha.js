
//var ccap = require('ccap');

exports.captchaGET = function (req, res) {
    var captcha = ccap({
        width: 200,
        height: 36,
        offset: 50,
        quality: 100,
        fontsize: 35
    });
    var ary = captcha.get();
//  this session captcha store way can't work well when do not use memory session
    req.session.captchaText = ary[0].toLowerCase();
    res.send(ary[1]);
};