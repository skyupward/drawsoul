
var hashTool = require('../../../utilities').hashTool;
var config = require('../../../config');
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var mailService = require('../../../service').mailService;
var UserDAO = require('../../../DAO').UserDAO;

var title = '注册';

exports.registerGET = function (req, res) {
    var templateData = {
        title: title,
        form: ""
    };
    res.render('auth/register/register', templateData);
}

exports.registerPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var templateData = {
        title: title,
        form: form
    };

//    validate
//    var captcha = sanitize(form.captcha).trim();
//    if(captcha.toLowerCase() != req.session.captchaText){
//        templateData.error = '输入正确的验证码';
//        res.render('auth/register/register', templateData);
//        return;
//    }

    var name = sanitize(form.name).trim();
    name = sanitize(name).xss();
    try {
        check(name).len(1, 18);
    } catch (e) {
        templateData.error = '昵称不超过18个字';
        res.render('auth/register/register', templateData);
        return;
    }

    var email = sanitize(form.email).trim();
    email = sanitize(email).xss();
    email = email.toLowerCase();
    try {
        check(email).isEmail();
    } catch (e) {
        templateData.error = '请输入正确的邮箱';
        res.render('auth/register/register', templateData);
        return;
    }

    var password = sanitize(form.password).trim();
    password = sanitize(password).xss();

    var passwordAgain = sanitize(form.passwordAgain).trim();
    passwordAgain = sanitize(passwordAgain).xss();

    try {
        check(password).len(6, 18);
    } catch (e) {
        templateData.error = '密码长度为6-18位';
        res.render('auth/register/register', templateData);
        return;
    }

    if (password === passwordAgain) {
        UserDAO.findUserByEmail(email, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                templateData.error = '此邮箱已被占用';
                res.render('auth/register/register', templateData);
            } else {
                UserDAO.findUserByName(name, function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {
                        templateData.error = '昵称已被占用';
                        res.render('auth/register/register', templateData);
                    } else {
                        var userRaw = new Object();
                        userRaw.email = email;
                        userRaw.name = name;
                        userRaw.password = hashTool.hmacSha256Hex(email, password);
                        userRaw.portrait = 'default';//use qiniu
//                        userRaw.portrait = config.PORTRAIT_DEFAULT_PATH;
                        UserDAO.newUser(userRaw, function (err, user) {
                            if (err) {
                                return next(err);
                            }
                            var token = hashTool.hmacSha256Hex(user.id, config.REGISTER_SECRET_KEY) + user._id;
                            mailService.sendActivationEmail(email, name, token, function(err){
                                if(err){
                                    return next(err);
                                }
                                templateData.success = '一封激活邮件已经发到您的邮箱';
                                res.render('auth/register/register', templateData);
                            });
                        });
                    }
                });
            }
        });
    } else {
        templateData.error = '两次输入的密码不一致';
        res.render('auth/register/register', templateData);
    }
}