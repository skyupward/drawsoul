
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var hashTool = require('../../../utilities').hashTool;
var stringTool = require('../../../utilities').stringTool;
var UserDAO = require('../../../DAO').UserDAO;

var title = "重置密码";

exports.resetPasswordGET = function (req, res, next) {
    if (!stringTool.regResetPasswordToken(req.params.token)) {
        next();
        return;
    }
    var id = req.params.token.slice(-24);
    var token = req.params.token.slice(0, 64);
    UserDAO.findUserById(id, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user && (user.resetPasswordToken === token) && (user.resetPasswordExpires > new Date())) {
            var templateData = {
                title: title,
                token: req.params.token
            };
            res.render('auth/reset_password/reset_password', templateData);
        } else {
            next();
        }
    });

};

exports.resetPasswordPOST = function (req, res, next) {
    if (!stringTool.regResetPasswordToken(req.params.token)) {
        next();
        return;
    }
    var id = req.params.token.slice(-24);
    var token = req.params.token.slice(0, 64);
    UserDAO.findUserById(id, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user && (user.resetPasswordToken === token) && (user.resetPasswordExpires > new Date())) {
            var form = req.body.form;
            if (!form) {
                return next();
            }
            var templateData = {
                title: title,
                token: req.params.token
            };
            var password = sanitize(form.password).trim();
            password = sanitize(password).xss();

            var passwordAgain = sanitize(form.passwordAgain).trim();
            passwordAgain = sanitize(passwordAgain).xss();

            try {
                check(password).len(6, 18);
            } catch (e) {
                templateData.error = '密码长度为6-18位';
                res.render('auth/reset_password/reset_password', templateData);
                return;
            }
            if (password === passwordAgain) {
                user.password = hashTool.hmacSha256Hex(user.email, password);
                user.resetPasswordExpires = new Date();
                UserDAO.saveUser(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    templateData.success = '密码已重置，请登录';
                    res.render('auth/reset_password/reset_password', templateData);
                });
            } else {
                templateData.error = '两次输入的密码不一致';
                res.render('auth/reset_password/reset_password', templateData);
            }
        } else {
            next();
        }
    });
};