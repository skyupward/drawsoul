
exports.loginGET = require('./login').loginGET;
exports.loginPOST = require('./login').loginPOST;
exports.logoutGET = require('./logout').logoutGET;
exports.registerGET = require('./register').registerGET;
exports.registerPOST = require('./register').registerPOST;
exports.forgetPasswordGET = require('./forget_password').forgetPasswordGET;
exports.forgetPasswordPOST = require('./forget_password').forgetPasswordPOST;
exports.activateAccountGET = require('./activate_account').activateAccountGET;
exports.resetPasswordGET = require('./reset_password').resetPasswordGET;
exports.resetPasswordPOST = require('./reset_password').resetPasswordPOST;
exports.captchaGET = require('./captcha').captchaGET;
