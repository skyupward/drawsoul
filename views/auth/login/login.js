
var hashTool = require('../../../utilities').hashTool;
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var UserDAO = require('../../../DAO').UserDAO;

var title = '登录';

exports.loginGET = function (req, res) {
    var templateData = {
        title: title,
        form: ""
    };
    res.render('auth/login/login', templateData);
}

exports.loginPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var templateData = {
        title: title,
        form: form
    };
    if (form.remember) {
        form.remember = 'checked';
    } else {
        form.remember = '';
    }

    var email = sanitize(form.email).trim().toLowerCase();
    try {
        check(email).isEmail();
    } catch (e) {
        templateData.error = '请输入正确的邮箱';
        res.render('auth/login/login', templateData);
        return;
    }
    var password = sanitize(form.password).trim();
    try {
        check(password).len(6, 18);
    } catch (e) {
        templateData.error = '密码长度为6-18位';
        res.render('auth/login/login', templateData);
        return;
    }
    UserDAO.findUserByEmail(email, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            password = hashTool.hmacSha256Hex(email, password);
            if (user.password === password) {
                if (user.activated) {
                    req.session.regenerate(function (err) {
                        if (err) {
                            return next(err);
                        }
                        user.lastLoginTime = new Date();
                        UserDAO.saveUser(user, function (err, user) {
                            if (err) {
                                return next(err);
                            }
                            if (form.remember) {
                                req.session.cookie.maxAge = 1000 * 60 * 60 * 24 * 15;
                                //don't know why above sentence didn't refresh browser cookie's expires,so rewrite it explicitly
                                res.cookie('sid', req.cookies.sid, {maxAge: 1000 * 60 * 60 * 24 * 15});
                            } else {
                                req.session.cookie.expires = false;
                            }
                            req.session.user = user;
                            res.redirect('/');
                        });
                    });
                } else {
                    templateData.error = '该账号未激活，请先到' + email + '激活';
                    res.render('auth/login/login', templateData);
                }
            } else {
                templateData.error = '登陆名或密码错误';
                res.render('auth/login/login', templateData);
            }
        } else {
            templateData.error = '用户不存在';
            res.render('auth/login/login', templateData);
        }
    });
};
