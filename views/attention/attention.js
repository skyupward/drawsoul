
var AttentionDAO = require('../../DAO').AttentionDAO;
var UserDAO = require('../../DAO').UserDAO;
var stringTool = require('../../utilities').stringTool;

exports.attentionGET = function (req, res, next) {

    var attentionManId = req.query.attentionMan;
    if (!stringTool.regMongoDBId(attentionManId)) {
        return next();
    }

    AttentionDAO.findAttentionByCreatorIdByAttentionManId(req.session.user._id, attentionManId, function (err, attention) {
        if (err) {
            return next(err);
        }
        if (attention) {
            res.json({attention: true});
        } else {
            res.json({attention: false});
        }
    });
};
exports.attentionPOST = function (req, res, next) {

    var attentionManId = req.body.attentionMan;
    if (!stringTool.regMongoDBId(attentionManId)) {
        return next();
    }

    AttentionDAO.findAttentionByCreatorIdByAttentionManId(req.session.user._id, attentionManId, function (err, attention) {
        if (err) {
            return next(err);
        }
        var action = req.body.action;
        if(action === 'attention' && !attention){
            var attentionRaw = {
                _attentionMan: attentionManId,
                _creator: req.session.user._id
            };
            AttentionDAO.newAttention(attentionRaw, function (err) {
                if (err) {
                    return next(err);
                }
                UserDAO.incFollowersCountById(attentionManId, function (err) {
                    if (err) return next(err);
                    UserDAO.incAttentionCountById(req.session.user._id, function (err) {
                        if (err) return next(err);
                        res.json({attention: true});
                    });
                });
            });
        }else if(action === 'cancel' && attention){
            AttentionDAO.cancelAttentionByCreatorIdByAttentionManId(req.session.user._id, attentionManId, function (err) {
                if (err) {
                    return next(err);
                }
                UserDAO.decFollowersCountById(attentionManId, function (err) {
                    if (err) return next(err);
                    UserDAO.decAttentionCountById(req.session.user._id, function (err) {
                        if (err) return next(err);
                        res.json({attention: false});
                    });
                });
            });
        }else if(action === 'attention' && attention){
            res.json({attention: true});
        }else if(action === 'cancel' && !attention){
            res.json({attention: false});
        }else{
            next();
        }
    });
};
