
var setBasic = require('./set_basic');
var setExtra = require('./set_extra');
var setPassword = require('./set_password');
var setPortrait = require('./set_portrait');

exports.setBasicGET = setBasic.setBasicGET;
exports.setBasicPOST = setBasic.setBasicPOST;
exports.setExtraGET = setExtra.setExtraGET;
exports.setExtraPOST = setExtra.setExtraPOST;
exports.setPasswordGET = setPassword.setPasswordGET;
exports.setPasswordPOST = setPassword.setPasswordPOST;
exports.setPortraitGET = setPortrait.setPortraitGET;
exports.setPortraitPOST = setPortrait.setPortraitPOST;