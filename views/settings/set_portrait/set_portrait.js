
var fs = require('fs');
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var config = require('../../../config');
var UserDAO = require('../../../DAO').UserDAO;
var qiniuService = require('../../../service').qiniuService;

var title = "设置头像";

exports.setPortraitGET = function (req, res, next) {
    var user = req.session.user;
    var upToken = qiniuService.portraitUpToken();
    var fileKey = user._id + '/' + new Date().getTime();
    var templateData = {
        title: title,
        fileKey: fileKey,
        portrait: user.portrait,
        upToken: upToken
    };
    var ret = req.query.upload_ret;
    if (ret) {
        ret = qiniuService.restoreReturnBody(ret);
        try {
            ret = JSON.parse(ret);
        } catch (e) {
            return next(e);
        }
        if (ret.size === 0) {
            qiniuService.deletePortrait(ret.key, function () {
                templateData.error = '请选择头像文件';
                res.render('settings/set_portrait/set_portrait', templateData);
            }, next);
            return;
        }

        try {
            check(ret.type).isIn(config.IMG_TYPE);
        } catch (e) {
            qiniuService.deletePortrait(ret.key, function () {
                templateData.error = '只能上传png, jpeg, jpg, gif文件';
                res.render('settings/set_portrait/set_portrait', templateData);
            }, next);
            return;
        }

        if (ret.size > config.PORTRAIT_SIZE_LIMIT) {
            qiniuService.deletePortrait(ret.key, function () {
                templateData.error = '文件不能大于' + config.PORTRAIT_SIZE_LIMIT / 1024 + 'KB';
                res.render('settings/set_portrait/set_portrait', templateData);
            }, next);
            return;
        }

        var updateData = {
            portrait: ret.key
        };
        UserDAO.findUserByIdAndUpdate(user._id, updateData, function (err, user) {
            if (err) {
                return next(err);
            }
            req.session.user = user;
            templateData.portrait = user.portrait;
            templateData.success = '头像设置成功';
            res.render('settings/set_portrait/set_portrait', templateData);
        });
    } else {
        res.render('settings/set_portrait/set_portrait', templateData);
    }
};


//post no used if use qiniu
exports.setPortraitPOST = function (req, res, next) {

//    var form = req.files.form;
//    if (!form) {
//        return next();
//    }
//    var portrait = form.portrait;
//    var templateData = {
//        title: title,
//        form: ""
//    };
//
//    if (portrait.size === 0) {
//        templateData.error = '请选择头像文件';
//        res.render('settings/set_portrait/set_portrait', templateData);
//        return;
//    }
//
//    try {
//        check(portrait.type).isIn(config.IMG_TYPE);
//    } catch (e) {
//        templateData.error = '只能上传png, jpeg, jpg, gif文件';
//        res.render('settings/set_portrait/set_portrait', templateData);
//        return;
//    }
//
//    if (portrait.size > config.PORTRAIT_SIZE_LIMIT) {
//        templateData.error = '文件不能大于' + config.PORTRAIT_SIZE_LIMIT / 100 + 'KB';
//        res.render('settings/set_portrait/set_portrait', templateData);
//        return;
//    }
//
//    var tmp_path = portrait.path;
//    var target_dir = 'public/images/user/portrait/' + req.session.user._id;
//    var target_path = target_dir + '/' + new Date().getTime() + '_' + portrait.name;
//
//    function moveFileAndResponse() {
//        fs.rename(tmp_path, target_path, function (err) {
//            if (err) return next(err);
//            fs.unlink(tmp_path, function () {
//                if (err) return next(err);
//                UserDAO.findUserByIdAndUpdate(req.session.user._id, {portrait: target_path.slice(6)}, function (err, user) {
//                    if (err) {
//                        return next(err);
//                    }
//                    req.session.user = user;
//                    templateData.form = user;
//                    templateData.success = "设置成功";
//                    res.render('settings/set_portrait/set_portrait', templateData);
//                });
//            });
//        });
//    }
//
//    fs.exists(target_dir, function (exists) {
//        if (exists) {
//            moveFileAndResponse();
//        } else {
//            fs.mkdir(target_dir, function (err) {
//                if (err) return next(err);
//                moveFileAndResponse();
//            });
//        }
//    });
}