
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var UserDAO = require('../../../DAO').UserDAO;
var TagDAO = require('../../../DAO').TagDAO;

var title = "附加信息设置";

exports.setExtraGET = function (req, res) {
    var user = req.session.user;
    var tags = [];
    for (var i in user._tags) {
        tags.push(user._tags[i].name);
    }
    var templateData = {
        title: title,
        form: user,
        tags: tags
    };
    res.render('settings/set_extra/set_extra', templateData);
};

exports.setExtraPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var templateData = {
        title: title,
        form: form
    };
    var updateData = new Object();

    if (form.realName) {
        var realName = sanitize(form.realName).trim();
        realName = sanitize(realName).xss();
        try {
            check(realName).len(2, 18);
            updateData.realName = realName;
        } catch (e) {
            templateData.error = '姓名最短2个字，最长不超过18个字';
            res.render('settings/set_extra/set_extra', templateData);
            return;
        }
    } else {
        updateData.realName = "";
    }

    if (form.hometown) {
        var hometown = sanitize(form.hometown).trim();
        hometown = sanitize(hometown).xss();
        try {
            check(hometown).len(1, 25);
            updateData.hometown = hometown;
        } catch (e) {
            templateData.error = '故乡不超过25个字';
            res.render('settings/set_extra/set_extra', templateData);
            return;
        }
    } else {
        updateData.hometown = "";
    }

    if (form.school) {
        var school = sanitize(form.school).trim();
        school = sanitize(school).xss();
        try {
            check(school).len(1, 20);
            updateData.school = school;
        } catch (e) {
            templateData.error = "毕业院校不超过20个字";
            res.render('settings/set_extra/set_extra', templateData);
            return;
        }
    } else {
        updateData.school = "";
    }

    if (form.profession) {
        var profession = sanitize(form.profession).trim();
        profession = sanitize(profession).xss();
        try {
            check(profession).len(1, 15);
            updateData.profession = profession;
        } catch (e) {
            templateData.error = "职业不超过15个字";
            res.render('settings/set_extra/set_extra', templateData);
            return;
        }
    } else {
        updateData.profession = "";
    }

    if (form.company) {
        var company = sanitize(form.company).trim();
        company = sanitize(company).xss();
        try {
            check(company).len(1, 20);
            updateData.company = company;
        } catch (e) {
            templateData.error = "所在单位不超过20个字";
            res.render('settings/set_extra/set_extra', templateData);
            return;
        }
    } else {
        updateData.company = "";
    }

    if (form.tags) {
        var tags = sanitize(form.tags).trim();
        var tags = tags.split(" ");
        var tempObj = {}, tempArray = [];//these vars for omitting duplicated value in tags
        try {
            if (tags.length > 5) {
                throw new Error('标签数不大于5个');
            }
            for (var i in tags) {
                var tag = sanitize(tags[i]).trim();
                tag = sanitize(tag).xss();
                if (tag.length > 15) {
                    throw new Error('标签字数大于15');
                }
                if (tag.length === 0 || tempObj[tag]) {
                    continue;
                }
                tempObj[tag] = true;
                tempArray.push(tag);
            }
            updateData._tags = tempArray;
        } catch (e) {
            templateData.error = "每个标签最多15个字，最多贴5个标签";
            res.render('settings/set_extra/set_extra', templateData);
            return;
        }
    } else {
        updateData._tags = [];
    }

    var currentTags = req.session.user._tags;
    var tagsSize = updateData._tags.length + currentTags.length;

    //no tags need deal
    if (!tagsSize) {
        UserDAO.findUserByIdAndUpdate(req.session.user._id, updateData, function (err, user) {
            if (err) {
                return next(err);
            }
            req.session.user = user;
            templateData.success = "附加信息设置成功";
            res.render('settings/set_extra/set_extra', templateData);
        });
        return;
    }

    //else
    //just decrease current tags' using count and increase new tags' using count simply,
    //then set new tags to db
    var newTagIds = [];

    function done(_id) {
        if (_id) {
            newTagIds.push(_id);
        }
        if (--tagsSize) {
            return;
        }
        updateData._tags = newTagIds;
        UserDAO.findUserByIdAndUpdate(req.session.user._id, updateData, function (err, user) {
            if (err) {
                return next(err);
            }
            req.session.user = user;
            templateData.success = "附加信息设置成功";
            res.render('settings/set_extra/set_extra', templateData);
        });
    }

    currentTags.forEach(function (tag) {
        TagDAO.decUsingCountByName(tag.name, function (err) {
            if (err) {
                return next(err);
            }
            done();
        });
    });

    updateData._tags.forEach(function (tagName) {
        TagDAO.isExistByName(tagName, function (err, exist) {
            if (err) {
                return next(err);
            }
            if (exist) {
                TagDAO.incUsingCountByName(tagName, function (err, tag) {
                    if (err) {
                        return next(err);
                    }
                    done(tag._id);
                });
            } else {
                var tagRaw = {
                    name: tagName,
                    _creator: req.session.user._id
                };
                TagDAO.newTag(tagRaw, function (err, tag) {
                    if (err) {
                        return next(err);
                    }
                    done(tag._id);
                });
            }
        });
    });
};