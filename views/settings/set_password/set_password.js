
var createHmac = require('crypto').createHmac;
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var UserDAO = require('../../../DAO').UserDAO;

var title = "密码设置";

exports.setPasswordGET = function (req, res) {
    var templateData = {
        title: title
    };
    res.render('settings/set_password/set_password', templateData);
};

exports.setPasswordPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var user = req.session.user;
    var templateData = {
        title: title
    };
    var password = sanitize(form.password).trim();
    try {
        check(password).len(6, 18);
    } catch (e) {
        templateData.error = '密码长度为6-18位';
        res.render('settings/set_password/set_password', templateData);
        return;
    }
    password = createHmac('sha256', user.email).update(password).digest('hex');
    if (user.password === password) {
        var newPassword = sanitize(form.newPassword).trim();
        newPassword = sanitize(newPassword).xss();

        var passwordAgain = sanitize(form.passwordAgain).trim();
        passwordAgain = sanitize(passwordAgain).xss();

        try {
            check(newPassword).len(6, 18);
        } catch (e) {
            templateData.error = '密码长度为6-18位';
            res.render('settings/set_password/set_password', templateData);
            return;
        }

        if (newPassword === passwordAgain) {
            var obj = new Object();
            obj.password = createHmac('sha256', user.email).update(newPassword).digest('hex');
            UserDAO.findUserByIdAndUpdate(req.session.user._id, obj, function (err, user) {
                if (err) {
                    return next(err);
                }
                req.session.user = user;
                templateData.success = "密码已更新";
                res.render('settings/set_password/set_password', templateData);
            });
        } else {
            templateData.error = '两次输入的密码不一致';
            res.render('settings/set_password/set_password', templateData);
        }

    } else {
        templateData.error = '当前密码错误！';
        res.render('settings/set_password/set_password', templateData);
    }
};
