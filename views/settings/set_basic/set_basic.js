
var check = require('validator').check;
var sanitize = require('validator').sanitize;
var UserDAO = require('../../../DAO').UserDAO;

var title = "基本信息设置";

exports.setBasicGET = function (req, res) {
    var templateData = {
        title: title,
        form: req.session.user
    };
    res.render('settings/set_basic/set_basic', templateData);
};

exports.setBasicPOST = function (req, res, next) {
    var form = req.body.form;
    if (!form) {
        return next();
    }
    var templateData = {
        title: title,
        form: form
    };
    var updateData = {};

    var name = sanitize(form.name).trim();
    name = sanitize(name).xss();
    try {
        check(name).len(1, 18);
        updateData.name = name;
    } catch (e) {
        templateData.error = '请输入昵称,且昵称不超过18个字';
        res.render('settings/set_basic/set_basic', templateData);
        return;
    }

    if (form.birthday) {
        var birthday = sanitize(form.birthday).trim();
        birthday = sanitize(birthday).xss();
        try {
            check(birthday).isDate();
            updateData.birthday = birthday;
        } catch (e) {
            templateData.error = '生日正确格式如：1980-01-02';
            res.render('settings/set_basic/set_basic', templateData);
            return;
        }
    } else {
        updateData.birthday = "";
    }

    if (form.gender) {
        var gender = sanitize(form.gender).trim();
        if (gender != 'male' && gender != 'female') {
            templateData.error = "性别不合法";
            res.render('settings/set_basic/set_basic', templateData);
            return;
        } else {
            updateData.gender = gender;
        }
    } else {
        updateData.gender = "";
    }


    if (form.address) {
        var address = sanitize(form.address).trim();
        address = sanitize(address).xss();
        try {
            check(address).len(1, 25);
            updateData.address = address;
        } catch (e) {
            templateData.error = "所在地不超过25个字";
            res.render('settings/set_basic/set_basic', templateData);
            return;
        }
    } else {
        updateData.address = "";
    }


    if (form.selfInfo) {
        var selfInfo = sanitize(form.selfInfo).trim();
        selfInfo = sanitize(selfInfo).xss();
        try {
            check(selfInfo).len(1, 200);
            updateData.selfInfo = selfInfo;
        } catch (e) {
            templateData.error = "自我介绍不超过200个字";
            res.render('settings/set_basic/set_basic', templateData);
            return;
        }
    } else {
        updateData.selfInfo = "";
    }

    UserDAO.findUserByName(name, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user && user.id != req.session.user._id) {
            templateData.error = '昵称已被占用';
            res.render('settings/set_basic/set_basic', templateData);
        } else {
            UserDAO.findUserByIdAndUpdate(req.session.user._id, updateData, function (err, user) {
                if (err) {
                    return next(err);
                }
                req.session.user = user;
                templateData.success = "基本信息设置成功";
                res.render('settings/set_basic/set_basic', templateData);
            });
        }
    });
};