/**
 * Module dependencies.
 */

var express = require('express')
    , http = require('http')
    , path = require('path');

var fs = require('fs');
var middleware = require('./middleware');
var route = require('./route');
var MongoStore = require('connect-mongo')(express);
//var RedisStore = require('connect-redis')(express);
var mongoose = require('mongoose');
var config = require('./config');

var access_logfile = fs.createWriteStream(__dirname + '/logs/access.log', {flags: 'a'});
var app = express();

//app.disable('x-powered-by');

//connect mongodb
mongoose.connect(config.MONGODB_ADDR);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').__express);
app.set('view engine', 'html');

app.use(express.favicon(__dirname + '/public/images/app/favicon.ico'));
//app.use(express.logger('dev'));
app.use(express.logger({format: 'tiny', stream: access_logfile }));
app.use(express.compress());
app.use(express.bodyParser());
//app.use(express.bodyParser({uploadDir: './public/images'}));
app.use(express.methodOverride());
app.use(express.cookieParser(''));
//app.use(express.session({
//    secret: config.SECRET_KEY, store: new RedisStore(), key: "sid"
//}));
app.use(express.session({
    secret: config.SECRET_KEY, store: new MongoStore({
        db: config.MONGO_SESSION_DB_NAME
    }), key: "sid"
}));
//app.use(express.session({
//    secret: config.SECRET_KEY, key: "sid"
//}));
//custom middleware for offering current user to template in requst/response cycle
app.use(middleware.addCurrentUser);
//custom middleware for offering footer info variable in template render
app.use(middleware.addFooterInfo);
//custom middleware for offering current user's message count
app.use(middleware.queryMessageCount);
//custom middleware for decoding uri to show chinese
//app.use(middleware.decodeURI);
app.use(express.csrf());
//custom middleware for offering _csrf field's value in form
app.use(middleware.addCsrfToken);
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.use(middleware.error404);

//development only
//if ('development' == app.get('env')) {
//    app.use(express.errorHandler());
//}

//if ('production' == app.get('env')) {
    app.use(middleware.error500);
//}

//define route
route(app);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});