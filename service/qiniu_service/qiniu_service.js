
//var Buffer = require('Buffer');
var qiniu = require('qiniu');

qiniu.conf.ACCESS_KEY = 'xxx';
qiniu.conf.SECRET_KEY = 'xxx';

exports.portraitUpToken = function () {

    var putPolicy = new qiniu.rs.PutPolicy('xxx');
    putPolicy.returnUrl = 'http://xxx.com/set/portrait';
    putPolicy.returnBody ='{"key":$(x:key), "size":$(fsize), "type":$(mimeType)}';
    //putPolicy.asyncOps = asyncOps;
    //putPolicy.expires = expires;

    return putPolicy.token();
};

exports.imageUpToken = function () {

    var putPolicy = new qiniu.rs.PutPolicy('xxx-img');
    //putPolicy.callbackUrl = callbackUrl;
    //putPolicy.callbackBody = callbackBody;
    putPolicy.returnUrl = 'http://xxx.com/drawing/new';
    putPolicy.returnBody = '{"key":$(x:key), "size":$(fsize), "type":$(mimeType),' +
        ' "name":$(x:name), "album":$(x:album), "describe":$(x:describe), "tags":$(x:tags)}';
//    putPolicy.returnUrl = 'http://localhost:3000/drawing/new';
//    putPolicy.returnBody = '{"key":$(x:key), "size":$(fsize), "type":$(mimeType),' +
//        ' "name":$(x:name), "album":$(x:album), "describe":$(x:describe), "tags":$(x:tags)}';
    //putPolicy.asyncOps = asyncOps;
    //putPolicy.expires = expires;

    return putPolicy.token();
};

exports.PutExtra = function (params, mimeType, crc32, checkCrc) {
    this.params = params || {};
    this.mimeType = mimeType || null;
    this.crc32 = crc32 || null;
    this.checkCrc = checkCrc || 0;
};

exports.uploadFile = function (localFile, key, uptoken) {
    var extra = new qiniu.io.PutExtra();
    //extra.params = params;
    //extra.mimeType = mimeType;
    //extra.crc32 = crc32;
    //extra.checkCrc = checkCrc;

    qiniu.io.putFile(uptoken, key, localFile, extra, function(err, ret) {
        if(!err) {
            // 上传成功， 处理返回值
            console.log(ret.key, ret.hash);
            // ret.key & ret.hash
        } else {
            // 上传失败， 处理返回代码
            console.log(err);
            // http://docs.qiniu.com/api/put.html#error-code
        }
    });
};

exports.restoreReturnBody = function(encodedBody){
    var temp = encodedBody.replace(/_/g, '/').replace(/-/g, '+');
    return new Buffer(temp, 'base64').toString();
};

exports.deleteFileNoCallback = function(bucketName, key, errHandler){
    var client = new qiniu.rs.Client();
    client.remove(bucketName, key, function(err) {
        if (err) {
            console.log(err);
            errHandler(err);
        }
    })
};

exports.deletePortrait = function(key, callback ,errHandler){
    var client = new qiniu.rs.Client();
    client.remove('xxx-por', key, function(err) {
        if (!err) {
            callback();
        } else {
            errHandler(err);
        }
    })
};

exports.deleteImage = function(key, callback ,errHandler){
    var client = new qiniu.rs.Client();
    client.remove('xxx-img', key, function(err) {
        if (!err) {
            callback();
        } else {
            errHandler(err);
        }
    })
};

exports.deleteImages = function(keys, callback, errHandler){
    var paths = [];
    keys.forEach(function(key){
        paths.push(new qiniu.rs.EntryPath('xxx-img', key));
    });
    var client = new qiniu.rs.Client();
    client.batchDelete(paths, function(err, ret) {
        if (!err) {
            for (var i in ret) {
                if (ret[i].code !== 200) {
                    console.log(ret[i].code, ret[i].data);
                }
            }
            callback();
        } else {
            console.log(err);
            errHandler(err);
        }
    });
};
