
var nodemailer = require('nodemailer');
var config = require('../../config');

var smtpTransport = nodemailer.createTransport('SMTP', {
    host: "smtp.exmail.xxx.com", // hostname
//    secureConnection: true, // use SSL
    port: 25, // port for secure SMTP
    auth: {
        user: "your user",
        pass: "password"
    }
});

exports.sendActivationEmail = function (to, name, token, callback) {
    var mailOptions = {
        from: "xxx<account@xxx.com>", // sender address
        to: to, // list of receivers
        subject: "欢迎加入", // Subject line
//        text: "欢迎加入XXX,", // plaintext body
        html: "您好，<b>" + name + "</b>：<br/><br/>" +
            "欢迎您的加入XXX，" +
            "请点击<a href='http://" + config.SITE_ROOT_URL + "welcome/to/xxx/" + token + "'>激活链接</a>完成验证。<br/>" +
            "如果您并未进行相关操作，说明有人滥用了您的邮箱，您可以简单地删除此邮件，很抱歉对您造成了打扰。<br/><br/>" +
            "<a href='http://" + config.SITE_ROOT_URL + "'>XXX</a> 谨上。<br/><br/><hr/>" +
            "<i>此邮件由系统自动发送，请勿直接回复，如对此有疑问，请联系 feedback@xxx.com </i><br>"// html body
    }
    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, result) {
        callback(error, result);
        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
}

exports.sendResetPasswordEmail = function (to, name, token, callback) {
    var mailOptions = {
        from: "xxx<account@xxx.com>", // sender address
        to: to, // list of receivers
        subject: "重置密码", // Subject line
//        text: "重置密码", // plaintext body
        html: "您好，<b>" + name + "</b>：<br/><br/>" +
            "您正在申请重置密码，" +
            "请点击<a href='http://" + config.SITE_ROOT_URL + "reset/password/" + token + "'>重置链接</a>完成密码重置（24小时内有效）。<br/>" +
            "如果您并未进行相关操作，说明有人滥用了您的邮箱，您可以忽略此邮件，您的账号依旧安全。<br/><br/>" +
            "<a href='http://" + config.SITE_ROOT_URL + "'>画XXX</a> 谨上。<br/><br/><hr/>" +
            "<i>此邮件由系统自动发送，请勿直接回复，如对此有疑问，请联系 feedback@xxx.com </i><br>"// html body
    }
    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, result) {
        callback(error, result);
        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
}