
var DrawingComment = require('../model').DrawingComment;

exports.newComment = function (obj, callback) {
    DrawingComment.create(obj, callback);
};

exports.findCommentsByDrawingId = function (id, page, amount, callback) {
    DrawingComment.find({_drawing: id}, {},
        {skip: (page - 1) * amount, limit: amount, populate: '_creator _at'}, callback);
};

exports.getCountByDrawingId = function (id, callback) {
    DrawingComment.count({_drawing: id}, callback);
};

exports.findCommentById = function (id, callback) {
    DrawingComment.findById(id, {}, {populate: '_creator _at'}, callback);
}

exports.getCountBeforeTime = function (time, callback) {
    DrawingComment.count({createTime: {$lte: time}}, callback);
};

exports.populateAt = function (comment, callback) {
    comment.populate('_at', callback);
};

exports.removeCommentsByDrawingId = function (id, callback) {
    DrawingComment.remove({_drawing: id}, callback);
};

