
var Like = require('../model').Like;

exports.newLike = function (attentionRaw, callback) {
    Like.create(attentionRaw, callback);
};

exports.cancelLikeByUserIdByDrawingId = function (userId, drawingId, callback) {
    Like.findOneAndRemove({_drawing: drawingId, _creator: userId}, callback);
};

exports.findLikeByUserIdByDrawingId = function (userId, drawingId, callback) {
    Like.findOne({_drawing: drawingId, _creator: userId}, callback)
};

exports.getCountByDrawingId = function (id, callback) {
    Like.count({_drawing: id}, callback);
};

exports.removeLikesByDrawingId = function (id, callback) {
    Like.remove({_drawing: id}, callback);
};