
var Album = require('../model').Album;

exports.newAlbum = function (obj, callback) {
    Album.create(obj, callback);
};

exports.saveAlbum = function (album, callback) {
    album.save(callback);
};

exports.findAlbumsByUserId = function (id, callback) {
    Album.find({_creator: id}, callback);
};

exports.findAlbumsByUserIdByPage = function (id, page, amount, callback) {
    Album.find({_creator: id}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.findAlbumById = function (id, callback) {
    Album.findById(id).populate('_creator _tags').exec(callback);
};

exports.findAlbumByIdAndUpdate = function (id, update, callback) {
    Album.findByIdAndUpdate(id, update, {populate: '_tags'}, callback);
};

exports.findAlbumsByPage = function (page, amount, callback) {
    Album.find({}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.findAlbumsByTag = function (tag, page, amount, callback) {
    Album.find({_tags: {$in: [tag]}}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.removeAlbumById = function (id, callback) {
    Album.findByIdAndRemove(id, callback);
};