
var AtMessage = require('../model').AtMessage;

exports.newMessage = function (messageRaw, callback) {
    AtMessage.create(messageRaw, callback);
};

exports.findMessagesByUserId = function (id, callback) {
    AtMessage.find({_to: id}, {}, {populate: '_from _drawing _drawingComment'}, callback);
};

exports.findUnreadMessagesByUserId = function (id, page, amount, callback) {
    AtMessage.find({_to: id, read: false}, {}, {skip: (page - 1) * amount, limit: amount, populate: '_from _drawing _drawingComment'}, callback);
};

exports.findReadMessagesByUserId = function (id, page, amount, callback) {
    AtMessage.find({_to: id, read: true}, {}, {skip: (page - 1) * amount, limit: amount, populate: '_from _drawing _drawingComment'}, callback);
};

exports.getUnreadMessagesCountByUserId = function (id, callback) {
    AtMessage.count({_to: id, read: false}, callback);
};

exports.getReadMessagesCountByUserId = function (id, callback) {
    AtMessage.count({_to: id, read: true}, callback);
};

exports.findMessageByIdAndSetRead = function (id, callback) {
    AtMessage.findByIdAndUpdate(id, {read: true}, callback);
};

exports.removeMessagesByDrawingId = function (id, callback) {
    AtMessage.remove({_drawing: id}, callback);
};

exports.removeMessagesByToUserId = function (id, callback) {
    AtMessage.remove({_to: id}, callback);
};

exports.removeMessageById = function (id, callback) {
    AtMessage.findByIdAndRemove(id, callback);
};
