
var User = require('../model').User;

exports.findUserById = function (id, callback) {
    User.findById(id, {}, {populate: "_tags"}, callback);
};

exports.findUserByIdAndUpdate = function (id, update, callback) {
    User.findByIdAndUpdate(id, update, {populate: '_tags'}, callback);
};

exports.findUserByEmail = function (email, callback) {
    User.findOne({ email: email }, {}, {populate: "_tags"}, callback);
};

exports.findUserByName = function (name, fields, options, callback) {
    User.findOne({ name: name }, fields, options, callback);
};

exports.findUsersByPage = function (page, amount, callback) {
    User.find({}, {}, {skip: (page - 1) * amount, limit: amount,
        sort: {followersCount: -1, paintingCount: -1, lastLoginTime: -1}}, callback);
}

exports.newUser = function (userRaw, callback) {
    var user = new User();
    for (var key in userRaw) {
        user[key] = userRaw[key];
    }
    user.save(callback);
}

exports.saveUser = function (user, callback) {
    user.save(callback);
}

exports.findUsersByTag = function (tag, page, amount, callback) {
    User.find({_tags: {$in: [tag]}}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.incFollowersCountById = function (id, callback) {
    User.findByIdAndUpdate(id, {$inc: {followersCount: 1}}, callback);
};

exports.decFollowersCountById = function (id, callback) {
    User.findByIdAndUpdate(id, {$inc: {followersCount: -1}}, callback);
};

exports.incAttentionCountById = function (id, callback) {
    User.findByIdAndUpdate(id, {$inc: {attentionCount: 1}}, callback);
};

exports.decAttentionCountById = function (id, callback) {
    User.findByIdAndUpdate(id, {$inc: {attentionCount: -1}}, callback);
};

exports.incPaintingCountById = function (id, callback) {
    User.findByIdAndUpdate(id, {$inc: {paintingCount: 1}}, callback);
};

exports.decPaintingCountById = function (id, callback) {
    User.findByIdAndUpdate(id, {$inc: {paintingCount: -1}}, callback);
};
