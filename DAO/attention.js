
var Attention = require('../model').Attention;

exports.newAttention = function (attentionRaw, callback) {
    Attention.create(attentionRaw, callback);
};

exports.cancelAttentionByCreatorIdByAttentionManId = function (creatorId, attentionManId, callback) {
    Attention.findOneAndRemove({_attentionMan: attentionManId, _creator: creatorId}, callback);
};

exports.findAttentionByCreatorIdByAttentionManId = function (creatorId, attentionManId, callback) {
    Attention.findOne({_attentionMan: attentionManId, _creator: creatorId}, callback)
};

exports.findAttentionsByUserIdByPage = function (id, page, amount, callback) {
    Attention.find({_creator: id}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}, populate: '_attentionMan'}, callback);
};

exports.findFansByUserIdByPage = function (id, page, amount, callback) {
    Attention.find({_attentionMan: id}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}, populate: "_creator"}, callback);
};