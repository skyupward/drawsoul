
var PersonalChat = require('../model').PersonalChat;

exports.newChatRecord = function (messageRaw, callback) {
    PersonalChat.create(messageRaw, callback);
};

exports.findChatRecordsIn30Days = function (talker1, talker2, page, amount, callback) {
    PersonalChat.find({ $or: [
            {_from: talker1, _to: talker2},
            { _from: talker2, _to: talker1 }
        ], createTime: { $gte: (new Date()).getDate() - 30}},
        {}, {populate: '_from', skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.findChatRecordById = function (id, callback) {
    PersonalChat.findById(id, {}, {populate: '_from'}, callback);
};

exports.getCountIn30Days = function (callback) {
    PersonalChat.count({createTime: { $gte: (new Date()).getDate() - 30}}, callback);
};
