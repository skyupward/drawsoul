
var Drawing = require('../model').Drawing;

exports.newDrawing = function (drawingRaw, callback) {
    Drawing.create(drawingRaw, callback);
};

exports.saveDrawing = function (drawing, callback) {
    drawing.save(callback);
}

exports.findDrawingsByPage = function (page, amount, callback) {
    Drawing.find({}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}, populate: "_creator"}, callback);
};

exports.findHotDrawingsByPage = function (page, amount, callback) {
    Drawing.find({}, {}, {skip: (page - 1) * amount, limit: amount, sort: {collectCount: -1, likeCount: -1, commentCount: -1, createTime: -1}, populate: "_creator"}, callback);
};

exports.findDrawingsByUserIdByPage = function (id, page, amount, callback) {
    Drawing.find({_creator: id}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.findDrawingsByAlbumIdByPage = function (id, page, amount, callback) {
    Drawing.find({_album: id}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.findDrawingsByAlbumId = function (id, callback) {
    Drawing.find({_album: id}, callback);
};

exports.findDrawingById = function (id, callback) {
    Drawing.findById(id, {}, {populate: '_tags _album'}, callback);
};

exports.findDrawingsByTag = function (tag, page, amount, callback) {
    Drawing.find({_tags: {$in: [tag]}}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}}, callback);
};

exports.removeDrawingById = function (id, callback) {
    Drawing.findByIdAndRemove(id, callback);
};

exports.incLikeCountById = function (id, callback) {
    Drawing.findByIdAndUpdate(id, {$inc: {likeCount: 1}}, callback);
};

exports.decLikeCountById = function (id, callback) {
    Drawing.findByIdAndUpdate(id, {$inc: {likeCount: -1}}, callback);
};

exports.incCommentCountById = function (id, callback) {
    Drawing.findByIdAndUpdate(id, {$inc: {commentCount: 1}}, callback);
};

exports.decCommentCountById = function (id, callback) {
    Drawing.findByIdAndUpdate(id, {$inc: {commentCount: -1}}, callback);
};

exports.incCollectCountById = function (id, callback) {
    Drawing.findByIdAndUpdate(id, {$inc: {collectCount: 1}}, callback);
};

exports.decCollectCountById = function (id, callback) {
    Drawing.findByIdAndUpdate(id, {$inc: {collectCount: -1}}, callback);
};
