
var Collection = require('../model').Collection;

exports.newCollection = function (attentionRaw, callback) {
    Collection.create(attentionRaw, callback);
};

exports.cancelCollectionByUserIdByDrawingId = function (userId, drawingId, callback) {
    Collection.findOneAndRemove({_drawing: drawingId, _creator: userId}, callback);
};

exports.findCollectionByUserIdByDrawingId = function (userId, drawingId, callback) {
    Collection.findOne({_drawing: drawingId, _creator: userId}, callback)
};

exports.getCountByDrawingId = function (id, callback) {
    Collection.count({_drawing: id}, callback);
};

exports.findCollectionsByUserIdByPage = function (id, page, amount, callback) {
    Collection.find({_creator: id}, {}, {skip: (page - 1) * amount, limit: amount, sort: {createTime: -1}, populate: '_drawing'}, callback);
};

exports.removeCollectionsByDrawingId = function (id, callback) {
    Collection.remove({_drawing: id}, callback);
};