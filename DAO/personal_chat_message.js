
var PersonalChatMessage = require('../model').PersonalChatMessage;

exports.newMessage = function (messageRaw, callback) {
    PersonalChatMessage.create(messageRaw, callback);
};

exports.saveMessage = function (message, callback) {
    message.save(callback);
};

exports.findMessageByTalkers = function (from, to, callback) {
    PersonalChatMessage.findOne({ _from: from, _to: to }, callback);
};

exports.findUnreadMessagesByUserId = function (id, page, amount, callback) {
    PersonalChatMessage.find({_to: id, read: false}, {}, {skip: (page - 1) * amount, limit: amount, populate: '_from _personalChat'}, callback);
};

exports.findReadMessagesByUserId = function (id, page, amount, callback) {
    PersonalChatMessage.find({_to: id, read: true}, {}, {skip: (page - 1) * amount, limit: amount, populate: '_from _personalChat'}, callback);
};

exports.getUnreadMessagesCountByUserId = function (id, callback) {
    PersonalChatMessage.count({_to: id, read: false}, callback);
};

exports.getReadMessagesCountByUserId = function (id, callback) {
    PersonalChatMessage.count({_to: id, read: true}, callback);
};

exports.findMessageByIdAndSetRead = function (id, callback) {
    PersonalChatMessage.findByIdAndUpdate(id, {read: true}, callback);
};

exports.removeMessagesByToUserId = function (id, callback) {
    PersonalChatMessage.remove({_to: id}, callback);
};

exports.removeMessageById = function (id, callback) {
    PersonalChatMessage.findByIdAndRemove(id, callback);
};