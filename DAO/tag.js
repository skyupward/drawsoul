
var Tag = require('../model').Tag;

exports.newTag = function (obj, callback) {
    Tag.create(obj, callback);
};

exports.findTagById = function (id, callback) {
    Tag.findById(id, callback);
};

exports.findTagByName = function (name, callback) {
    Tag.findOne({name: name}, callback);
};

exports.getCount = function (callback) {
    Tag.count(callback);
};

exports.findTags = function (page, amount, callback) {
    Tag.find({}, {}, {skip: (page - 1) * amount, limit: amount, sort: {usingCount: -1}}, callback);
};

exports.isExistByName = function (name, callback) {
    Tag.count({name: name}, callback);
};

exports.isExistById = function (id, callback) {
    Tag.count({_id: id}, callback);
};

exports.incUsingCountByName = function (name, callback) {
    Tag.findOneAndUpdate({name: name}, {$inc: {usingCount: 1}}, callback);
};

exports.decUsingCountByName = function (name, callback) {
    Tag.findOneAndUpdate({name: name}, {$inc: {usingCount: -1}}, callback);
    ;
};