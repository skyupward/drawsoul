
var DrawingCommentMessage = require('../model').DrawingCommentMessage;

exports.newMessage = function (messageRaw, callback) {
    DrawingCommentMessage.create(messageRaw, callback);
};

exports.findMessagesByUserId = function (id, callback) {
    DrawingCommentMessage.find({_to: id}, {}, {populate: '_from _drawing _drawingComment'}, callback);
};

exports.findUnreadMessagesByUserId = function (id, page, amount, callback) {
    DrawingCommentMessage.find({_to: id, read: false}, {}, {skip: (page - 1) * amount, limit: amount, populate: '_from _drawing _drawingComment'}, callback);
};

exports.findReadMessagesByUserId = function (id, page, amount, callback) {
    DrawingCommentMessage.find({_to: id, read: true}, {}, {skip: (page - 1) * amount, limit: amount, populate: '_from _drawing _drawingComment'}, callback);
};

exports.getUnreadMessagesCountByUserId = function (id, callback) {
    DrawingCommentMessage.count({_to: id, read: false}, callback);
};

exports.getReadMessagesCountByUserId = function (id, callback) {
    DrawingCommentMessage.count({_to: id, read: true}, callback);
};

exports.findMessageByIdAndSetRead = function (id, callback) {
    DrawingCommentMessage.findByIdAndUpdate(id, {read: true}, callback);
};

exports.removeMessagesByDrawingId = function (id, callback) {
    DrawingCommentMessage.remove({_drawing: id}, callback);
};

exports.removeMessagesByToUserId = function (id, callback) {
    DrawingCommentMessage.remove({_to: id}, callback);
};

exports.removeMessageById = function (id, callback) {
    DrawingCommentMessage.findByIdAndRemove(id, callback);
};

