
module.exports = {

    APP_NAME: 'XXX',
    SITE_ROOT_URL: 'xxx.com/',

    DB_NAME: 'xxx',
    MONGODB_ADDR: 'mongodb://xxx',
    MONGO_SESSION_DB_NAME: 'xxx',

    SECRET_KEY: 'xxx',
    REGISTER_SECRET_KEY: 'xxx',


    IMG_TYPE: ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'],
    PORTRAIT_SIZE_LIMIT: 204800, //200KB
    DRAWING_SIZE_LIMIT: 2097152, //2M

    COMMENT_AMOUNT_PER_PAGE: 100,
    MESSAGE_AMOUNT_PER_PAGE: 25,
    CHAT_AMOUNT_PER_PAGE: 100,
    TAG_AMOUNT_PER_PAGE: 100,
    DRAWING_AMOUNT_PER_PAGE: 20,
    ALBUM_AMOUNT_PER_PAGE: 20,
    COLLECTION_AMOUNT_PER_PAGE: 20,
    PAINTER_AMOUNT_PER_PAGE: 100,

    MAX_AT_AMOUNT: 10

    //no used if use qiniu
//    ALBUM_DEFAULT_PATH: '/images/app/album_cover.png',
//    PORTRAIT_DEFAULT_PATH: '/images/app/face200.png'
};