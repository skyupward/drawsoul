
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var AtMessageSchema = new Schema({
    content: String,
    _from: {type: ObjectId, ref: "User"},
    _to: {type: ObjectId, ref: "User", index: true},
    _drawing: {type: ObjectId, ref: "Drawing", index: true},
    _drawingComment: {type: ObjectId, ref: "DrawingComment"},
    read: {type: Boolean, default: false},
    createTime: { type: Date, default: Date.now }
});
exports.AtMessage = mongoose.model('AtMessage', AtMessageSchema);