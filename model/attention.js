
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var AttentionSchema = new Schema({
    _creator: {type: ObjectId, ref: "User", index: true},
    _attentionMan: {type: ObjectId, ref: "User"},
    createTime: { type: Date, default: Date.now }
});
AttentionSchema.index({attentionMan: 1, _creator: 1});
exports.Attention = mongoose.model('Attention', AttentionSchema);