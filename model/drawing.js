
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var DrawingSchema = new Schema({
    name: String,
    path: String,
    describe: String,
    _creator: {type: ObjectId, ref: "User", index: true},
    _album: {type: ObjectId, ref: "Album", index: true},
    collectCount: {type: Number, default: 0},
    likeCount: {type: Number, default: 0},
    commentCount: {type: Number, default: 0},
    _tags: {type: [
        {type: ObjectId, ref: "Tag"}
    ], index: true},
    createTime: { type: Date, default: Date.now }
});
exports.Drawing = mongoose.model('Drawing', DrawingSchema);