
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var AlbumSchema = new Schema({
    name: String,
    describe: String,
    coverPath: String,
    _creator: {type: ObjectId, ref: "User", index: true},
    _tags: {type: [
        {type: ObjectId, ref: "Tag"}
    ], index: true},
    createTime: { type: Date, default: Date.now }
});
exports.Album = mongoose.model('Album', AlbumSchema);