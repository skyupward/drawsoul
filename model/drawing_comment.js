
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var DrawingCommentSchema = new Schema({
    _at: [
        {type: ObjectId, ref: "User"}
    ],
    content: String,
    _drawing: {type: ObjectId, ref: "Drawing", index: true},
    _creator: {type: ObjectId, ref: "User"},
    createTime: { type: Date, default: Date.now }
});
exports.DrawingComment = mongoose.model('DrawingComment', DrawingCommentSchema);