
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var UserSchema = new Schema({
    email: { type: String, unique: true },
    password: String,
    name: { type: String, unique: true },
    realName: String,
    birthday: String,
    gender: String,
    profession: String,
    school: String,
    company: String,
    address: String,
    hometown: String,
    selfInfo: String,
    portrait: String,
    _tags: {type: [
        {type: ObjectId, ref: "Tag"}
    ], index: true},
    paintingCount: {type: Number, default: 0},
    attentionCount: {type: Number, default: 0},
    followersCount: {type: Number, default: 0},
    createTime: { type: Date, default: Date.now },
    lastLoginTime: Date,
    activated: { type: Boolean, default: false},

    resetPasswordToken: String,
    resetPasswordExpires: Date
});
exports.User = mongoose.model('User', UserSchema);