
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var TagSchema = new Schema({
    name: {type: String},
    _creator: { type: ObjectId, ref: 'User'},
    usingCount: {type: Number, default: 1},
    createTime: { type: Date, default: Date.now }
});
exports.Tag = mongoose.model('Tag', TagSchema);
exports.TagSchema = TagSchema;