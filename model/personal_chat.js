
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var PersonalChatSchema = new Schema({
    content: String,
    _from: {type: ObjectId, ref: "User"},
    _to: {type: ObjectId, ref: "User"},
    createTime: { type: Date, default: Date.now }
});
PersonalChatSchema.index({ _from: 1, _to: 1});
exports.PersonalChat = mongoose.model('PersonalChat', PersonalChatSchema);
