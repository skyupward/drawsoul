
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var PersonalChatMessageSchema = new Schema({
    _from: {type: ObjectId, ref: "User"},
    _to: {type: ObjectId, ref: "User", index: true},
    _personalChat: {type: ObjectId, ref: "PersonalChat"},
    read: {type: Boolean, default: false},
    createTime: { type: Date, default: Date.now }
});
exports.PersonalChatMessage = mongoose.model('PersonalChatMessage', PersonalChatMessageSchema);