
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var CollectionSchema = new Schema({
    _drawing: {type: ObjectId, ref: "Drawing"},
    _creator: {type: ObjectId, ref: "User"},
    createTime: { type: Date, default: Date.now }
});
CollectionSchema.index({ _drawing: 1, _creator: 1});
exports.Collection = mongoose.model('Collection', CollectionSchema);