
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;
var LikeSchema = new Schema({
    _drawing: {type: ObjectId, ref: "Drawing"},
    _creator: {type: ObjectId, ref: "User"},
    createTime: { type: Date, default: Date.now }
});
LikeSchema.index({ _drawing: 1, _creator: 1});
exports.Like = mongoose.model('Like', LikeSchema);