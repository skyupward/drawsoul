$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    var $container = $('#gallery');
    var $initLoading = $("#init-loading");
    $container.imagesLoaded(function () {
        $initLoading.hide();
        $container.fadeIn();
        $container.masonry({
            itemSelector: '.span3',
            columnWidth: 220,
            gutter: 20
        });
    });
    $container.infinitescroll({
        loading: {
            finishedMsg: "",
            msgText: "<b>稍等一会，正在努力加载...</b>",
            img: "/images/app/loading.gif",
            selector : "#next-loading"
        },
        nextSelector: "div.pager a",
        navSelector: "div.pager",
        itemSelector: "div.span3"
    }, function (newElems) {
        $("#next-loading").hide();
        $("#last-loading").show();
        var $newElems = $(newElems).hide();
        $newElems.imagesLoaded(function () {
            $newElems.fadeIn();
            $container.masonry('appended', $newElems);
            $("#last-loading").hide();
            $("#next-loading").show();
        });
    });
    var colors = ["#faa755", "#b2d235", "#7bbfea", "#f391a9", "#dea32c", "#9b95c9", "#df9464"];
    $("a.album-tag").each(function () {
        $(this).css("background-color", colors[Math.floor(Math.random() * 7)]);
    });
});