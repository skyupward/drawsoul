$(document).ready(function () {
    $(".form").validate({
        rules: {
            "form[email]": {
                required: true,
                email: true
            },
            "form[password]": {
                required: true,
                rangelength: [6, 18]
            }
        },
        messages: {
            "form[email]": {
                required: "请输入登陆邮箱",
                email: "请输入正确的邮箱"
            },
            "form[password]": {
                required: "请输入密码",
                rangelength: "密码长度为6-18位"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});
