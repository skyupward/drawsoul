$(document).ready(function () {
    $(".form").validate({
        rules: {
            "form[email]": {
                required: true,
                email: true
            }
        },
        messages: {
            "form[email]": {
                required: "请输入登陆邮箱",
                email: "请输入正确的邮箱"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});