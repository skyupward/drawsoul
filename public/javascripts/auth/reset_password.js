$(document).ready(function () {
    $(".form").validate({
        rules: {
            "form[password]": {
                required: true,
                rangelength: [6, 18]
            },
            "form[passwordAgain]": {
                equalTo: "#password"
            }
        },
        messages: {
            "form[password]": {
                required: "请输入新密码",
                rangelength: "密码长度为6-18位"
            },
            "form[passwordAgain]": {
                equalTo: "两次输入的密码不一致"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});