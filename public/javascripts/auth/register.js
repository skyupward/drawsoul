$(document).ready(function () {
    $(".form").validate({
        rules: {
            "form[email]": {
                required: true,
                email: true
            },
            "form[name]": {
                required: true,
                rangelength: [1, 18]
            },
            "form[password]": {
                required: true,
                rangelength: [6, 18]
            },
            "form[passwordAgain]": {
                equalTo: "#password"
            },
            "form[captcha]": {
                required: true,
                rangelength: [4, 4]
            }
        },
        messages: {
            "form[email]": {
                required: "请输入登陆邮箱",
                email: "请输入正确的邮箱"
            },
            "form[name]": {
                required: "请输入昵称",
                rangelength: "昵称不超过18个字"
            },
            "form[password]": {
                required: "请输入密码",
                rangelength: "密码长度为6-18位"
            },
            "form[passwordAgain]": {
                equalTo: "两次输入的密码不一致"
            },
            "form[captcha]": {
                required: "请输入验证码",
                rangelength: "验证码为4位"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
    $("img").click(function () {
        $(this).attr("src", "/api/captcha");
    });
});

