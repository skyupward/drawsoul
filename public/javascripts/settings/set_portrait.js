$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    $("#nav-basic").removeClass("active");
    $("#nav-portrait").addClass("active");
    $(".form").validate({
        rules: {
            "file": {
                fileSize: 200,
                extension: {}
            }
        },
        messages: {
            "file": {
                extension: "只能上传png, jpeg, jpg, gif文件"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});