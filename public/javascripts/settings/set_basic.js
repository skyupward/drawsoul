$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    $(".form").validate({
        rules: {
            "form[name]": {
                required: true,
                rangelength: [1, 18]
            },
            "form[birthday]": {
                date: true
            },
            "form[address]": {
                rangelength: [1, 25]
            },
            "form[selfInfo]": {
                rangelength: [1, 200]
            }
        },
        messages: {
            "form[name]": {
                required: "请输入昵称",
                rangelength: "昵称不超过18个字"
            },
            "form[birthday]": {
                date: "生日正确格式如：1980-01-02"
            },
            "form[address]": {
                rangelength: "所在地不超过25个字"
            },
            "form[selfInfo]": {
                rangelength: "自我介绍不超过200个字"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
    setDatePickerZh();
    $("#birthday").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            dateFormat: "yy-mm-dd"
        }
    );
});