$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    $("#nav-basic").removeClass("active");
    $("#nav-extra").addClass("active");
    $(".form").validate({
        rules: {
            "form[realName]": {
                rangelength: [2, 18]
            },
            "form[hometown]": {
                rangelength: [1, 25]
            },
            "form[school]": {
                rangelength: [1, 20]
            },
            "form[profession]": {
                rangelength: [1, 15]
            },
            "form[company]": {
                rangelength: [1, 20]
            },
            "form[tags]": {
                tags: [15, 5]
            }
        },
        messages: {
            "form[realName]": {
                rangelength: "姓名最短2个字，最长不超过18个字"
            },
            "form[hometown]": {
                rangelength: "故乡不超过25个字"
            },
            "form[school]": {
                rangelength: "毕业院校不超过20个字"
            },
            "form[profession]": {
                rangelength: "职业不超过15个字"
            },
            "form[company]": {
                rangelength: "所在单位不超过20个字"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
    $(".form").submit(function (e) {
        var allEmpty = true;
        $("input[type='text']").each(function () {
            if ($(this).val()) {
                allEmpty = false;
            }
        });
        if (allEmpty) {
            e.preventDefault();
        }
    });
});