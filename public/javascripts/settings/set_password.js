$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    $("#nav-basic").removeClass("active");
    $("#nav-password").addClass("active");
    $(".form").validate({
        rules: {
            "form[password]": {
                required: true,
                rangelength: [6, 18]
            },
            "form[newPassword]": {
                required: true,
                rangelength: [6, 18]
            },
            "form[passwordAgain]": {
                equalTo: "#newPassword"
            }
        },
        messages: {
            "form[password]": {
                required: "请输入当前密码",
                rangelength: "密码长度为6-18位"
            },
            "form[newPassword]": {
                required: "请输入新密码",
                rangelength: "密码长度为6-18位"
            },
            "form[passwordAgain]": {
                equalTo: "两次输入的密码不一致"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});