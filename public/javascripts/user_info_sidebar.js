$(document).ready(function () {
    var action = "attention";
    var attentionMan = $("#attention").data("attention");
    if (attentionMan) {
        var csrfToken = $("#attention").data("csrf")
        $.ajax({
            url: "/api/attention",
            data: {
                attentionMan: attentionMan
            },
            success: function (data) {
                if (data.error || !data.attention) {
                    $("#attention").html('<i class="icon-user icon-white"></i> 加关注').removeAttr("disabled");
                } else if (data.attention) {
                    action = "cancel";
                    $("#attention").removeClass("btn-success").addClass("btn-danger").html("取消关注").removeAttr("disabled");
                }
            }
        });
        $("#attention").click(function () {
            $(this).attr("disabled", "ok");
            $.ajax({
                url: "/api/attention",
                type: "post",
                data: {
                    attentionMan: attentionMan,
                    action: action,
                    _csrf: csrfToken
                },
                success: function (data) {
                    if (data.error) {
                        location.href = "/login";
                    } else if (data.attention) {
                        action = "cancel";
                        $("#attention").removeClass("btn-success").addClass("btn-danger")
                            .html('<i class="icon-user icon-white"></i> 取消关注').removeAttr("disabled");
                        $("li .fans").text(parseInt($("li .fans").text()) + 1);
                    } else {
                        action = "attention";
                        $("#attention").removeClass("btn-danger").addClass("btn-success")
                            .html('<i class="icon-user icon-white"></i> 加关注').removeAttr("disabled");
                        $("li .fans").text(parseInt($("li .fans").text()) - 1);
                    }
                }
            });
        });
    }
    var colors = ["#faa755", "#b2d235", "#7bbfea", "#f391a9", "#dea32c", "#9b95c9", "#df9464"];
    $("a.tag").each(function () {
        $(this).css("background-color", colors[Math.floor(Math.random() * 7)]);
    });
});