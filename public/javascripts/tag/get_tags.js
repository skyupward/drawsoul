$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    $("#nav-tags").addClass("active");
    var colors = ["#faa755", "#b2d235", "#7bbfea", "#f391a9", "#dea32c", "#9b95c9", "#df9464"];
    $("a.tag").each(function () {
        $(this).css("background-color", colors[Math.floor(Math.random() * 7)]);
    });
});