$(document).ready(function () {
    $("#nav-gallery").removeClass("active");
    $(".form").validate({
        rules: {
            "form[content]": {
                rangelength: [1, 200]
            }
        },
        messages: {
            "form[content]": {
                rangelength: "私信不超过200个字"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});