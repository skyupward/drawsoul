$(document).ready(function () {
    $("#nav-gallery").removeClass("active")
    $(".form").validate({
        rules: {
            "form[at]": {
                atUser: 10
            },
            "form[content]": {
                required: true,
                rangelength: [1, 200]
            }
        },
        messages: {
            "form[content]": {
                required: "请输入留言内容",
                rangelength: "留言不超过200个字"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
    var colors = ["#faa755", "#b2d235", "#7bbfea", "#f391a9", "#dea32c", "#9b95c9", "#df9464"];
    $("a.drawing-tag").each(function () {
        $(this).css("background-color", colors[Math.floor(Math.random() * 7)]);
    });
    if ($("input[name='form[at]']").val())
        $("input[name='form[at]']").show();
    $("input[name='form[at]']").blur(function () {
        if (!$(this).val())
            $(this).hide();
    });
    $("form > a").click(function () {
        $("input[name='form[at]']").toggle();
    });
    var lAction = "like";
    var cAction = "collection"
    var drawing = $("#like").data("like");
    if (drawing) {
        var csrfToken = $("#like").data("csrf")
        $.ajax({
            url: "/api/like",
            data: {
                drawing: drawing
            },
            success: function (data) {
                if (!data.error) {
                    $("#like span").text(data.count < 0 ? 0 : data.count);
                    if (data.like) {
                        lAction = "cancel";
                        $("#like").button("toggle");
                    }
                }
                $("#like").removeAttr("disabled");
            }
        });
        $.ajax({
            url: "/api/collection",
            data: {
                drawing: drawing
            },
            success: function (data) {
                if (!data.error) {
                    $("#collection span").text(data.count < 0 ? 0 : data.count);
                    if (data.collection) {
                        cAction = "cancel";
                        $("#collection").button("toggle");
                    }
                }
                $("#collection").removeAttr("disabled");
            }
        });
        $("#like").click(function () {
            $(this).attr("disabled", "ok");
            $.ajax({
                url: "/api/like",
                type: "post",
                data: {
                    drawing: drawing,
                    action: lAction,
                    _csrf: csrfToken
                },
                success: function (data) {
                    if (data.error) {
                        location.href = "/login";
                    } else if (data.like) {
                        lAction = "cancel";
                        $("#like span").text(parseInt($("#like span").text()) + 1);
                    } else {
                        lAction = "like";
                        $("#like span").text(parseInt($("#like span").text()) - 1);
                    }
                    $("#like").removeAttr("disabled");
                }
            });
        });
        $("#collection").click(function () {
            $(this).attr("disabled", "ok");
            $.ajax({
                url: "/api/collection",
                type: "post",
                data: {
                    drawing: drawing,
                    action: cAction,
                    _csrf: csrfToken
                },
                success: function (data) {
                    if (data.error) {
                        location.href = "/login";
                    } else if (data.collection) {
                        cAction = "cancel";
                        $("#collection span").text(parseInt($("#collection span").text()) + 1);
                    } else {
                        cAction = "collection";
                        $("#collection span").text(parseInt($("#collection span").text()) - 1);
                    }
                    $("#collection").removeAttr("disabled");
                }
            });
        });
    }
});
function reply(name) {
    $("input[name='form[at]']").val(function (index, value) {
        $("#say textarea").focus();
        $("input[name='form[at]']").show();
        if (value.indexOf(name) != -1)
            return value;
        return value + " @" + name;
    });
}