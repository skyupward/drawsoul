$(document).ready(function () {
    $("#nav-gallery").removeClass("active")
    $('.selectpicker').selectpicker();
    $(".form").validate({
        rules: {
            "form[name]": {
                required: true,
                rangelength: [1, 20]
            },
            "form[describe]": {
                rangelength: [1, 200]
            },
            "form[tags]": {
                tags: [15, 5]
            }

        },
        messages: {
            "form[name]": {
                required: "请输入画名",
                rangelength: "画名不超过20个字"
            },
            "form[describe]": {
                rangelength: "描述不超过200个字"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
    $("#delete-ensure").click(function(){
        $(".modal-body p").html("<strong>正在处理您的操作，请稍等片刻...</strong>");
    });
});