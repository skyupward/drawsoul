$(document).ready(function () {
    $("#nav-gallery").removeClass("active")
    $('.selectpicker').selectpicker();
    $(".form").validate({
        rules: {
            "x:name": {
                required: true,
                rangelength: [1, 20]
            },
            "file": {
                fileSize: 2048,
                extension: {}
            },
            "x:describe": {
                rangelength: [1, 200]
            },
            "x:tags": {
                tags: [15, 5]
            }

        },
        messages: {
            "x:name": {
                required: "请输入画名",
                rangelength: "画名不超过20个字"
            },
            "file": {
                extension: "只能上传png, jpeg, jpg, gif文件"
            },
            "x:describe": {
                rangelength: "描述不超过200个字"
            }
        },
        errorContainer: ".alert-client",
        errorLabelContainer: ".alert-client ul",
        errorElement: "li"
    });
});