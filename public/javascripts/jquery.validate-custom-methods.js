$.validator.addMethod("tags", function (value) {
    var tags =  $.trim(value).split(" ");
    if (tags.length > 5) {
        return false;
    }
    for (var i in tags) {
        if (tags[i].length > 15) {
            return false;
        }
    }
    return true;
}, $.validator.format("每个标签最多{0}字，最多贴{1}个标签"));

$.validator.addMethod("fileSize", function (value, element, param) {
    if (!element.files.length) {
        return false;
    }
    var size = element.files[0].size;
    if (Math.ceil(size / 1024) > param) {
        return false;
    }
    return true;
}, $.validator.format("请选择图片文件，不能大于{0}KB"));

$.validator.addMethod("atUser", function (value) {
    value = $.trim(value);
    if(value.length == 0){
        return true;
    }
    var at = value.split(" ");
    if (at.length > 10) {
        return false;
    }
    for (var i in at) {
        if (at[i].slice(0, 1) != "@") {
            return false;
        }
    }
    return true;
}, $.validator.format("最多@{0}个用户,格式为“@xxx @xxx”（空格隔开）"));